import {Requirement} from "./Requirement";

export interface Job {
    id: string;
    createdAt: string;
    title: string;
    description: string;
    street: string;
    streetNumber: number;
    postalCode: number;
    city: string;
    country: string;
    salary: string;
    requirements: Requirement[];
    skillIds: string[];
    location: {latitude: number, longitude: number}
}
