import {Job} from "./Job";

export interface Employer {
    id: string;
    companyName: string;
    email: string;
    phone: string;
    jobs: Job[]
}