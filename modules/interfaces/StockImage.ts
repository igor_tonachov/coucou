export interface StockImage {
    id: string;
    imageUrl: string;
    thumbnailUrl: string;
}