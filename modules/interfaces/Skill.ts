export interface Skill {
    id: string;
    title: { languageCode: string; content: string; }[];
    description: { languageCode: string; content: string; }[];
    image: string;
    imageUrl: string;
}