export interface Requirement {
    id: number;
    skillId: number;
    title: { languageCode: string; content: string; }[];
    rating: number;
    subRequirements:Requirement[];
}