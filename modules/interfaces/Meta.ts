export interface Meta {
    last: boolean;
    totalElements: number;
    totalPages: number;
    sort: any;
    first: boolean;
    numberOfElements: number;
    size: number;
    number: number;
}