import {SkillControllerApi, SkillSelectionRequestDto} from "../../../rest/api";
import {basePath} from "../constants/api";
import {Skill} from "../../../rest";

const skillControllerApi = new SkillControllerApi({basePath});

export class SkillsApiService {
    static getAll(page: number, size: number = 8): Promise<any> {
        return skillControllerApi.getSkillsUsingGET(page, size);
    }

    static getSkillsBySearch(search: string, excludeSkillIds:Array<number>=[], page: number = 0, size: number = 8) {
        return skillControllerApi.searchSkillsUsingGET(search, excludeSkillIds, page, size);
    }

    static getSkillsForJob(jobId: number, page: number, size: number = 8): Promise<any> {
        return skillControllerApi.getSkillsForJobUsingGET(jobId, page, size);
    }

    static create(skill: any, options?: any) {
        return skillControllerApi.createSkillUsingPOST(skill, options);
    }

    static select(requestDto?: SkillSelectionRequestDto, options?: any) {
        return skillControllerApi.selectSkillUsingPOST(requestDto, options);
    }

    static update(id, skill): Promise<Skill> {
        return skillControllerApi.updateSkillUsingPATCH(id, skill);
    }

    static removeAll(ids) {
        return skillControllerApi.deleteSkillsUsingDELETE(ids);
    }

    static addSubSkill(id: number, subSkillId?: number, options?: any) {
        return skillControllerApi.addSubSkillUsingPOST(id, subSkillId, options)
    }

    static addParentSkill(id: number, parentSkillId?: number, options?: any) {
        return skillControllerApi.addParentSkillUsingPOST(id, parentSkillId, options)
    }

    static removeSubSkill(id: number, subSkillId?: number, options?: any) {
        return skillControllerApi.removeSubSkillUsingDELETE(id, subSkillId, options)
    }

    static removeParentSkill(id: number, parentSkillId?: number, options?: any) {
        return skillControllerApi.removeParentSkillUsingDELETE(id, parentSkillId, options)
    }

    static getSkillDetails(id: number, options?: any) {
        return skillControllerApi.getSkillUsingGET(id, options)
    }
}