import {ImageControllerApi} from "../../../rest/api";
import {basePath} from "../constants/api";

const imageControllerApi = new ImageControllerApi({basePath});


export class ImageApiService {
    static getStockImages(query?: string) {
        return imageControllerApi.searchImagesUsingGET(query, {});
    }
}