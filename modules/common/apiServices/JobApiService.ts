import {JobControllerApi, JobCreation} from "../../../rest/api";
import {basePath} from "@Common";


const jobControllerApi = new JobControllerApi({basePath});

export class JobApiService {

    static getById(id) {
        return jobControllerApi.getJobUsingGET(id);
    }

    static create(job?: JobCreation) {
        return jobControllerApi.createJobUsingPOST(job);
    }

    static update(id: number, job: JobCreation) {
        return jobControllerApi.updateJobUsingPATCH(id, job);
    }

    static addSkill(id: number, skillId: number, options?: any) {
        return jobControllerApi.addRequirementUsingPOST(id, skillId, options);
    }

    static addSubSkill(id: number, requirementId: number, skillId: number, options?: any) {
        return jobControllerApi.addSubRequirementUsingPOST(id, requirementId, skillId, options);
    }

    static removeSkill(id: number, requirementId: number, options?: any) {
        return jobControllerApi.removeRequirementFromJobUsingDELETE(id, requirementId, options);
    }

    static removeAll(ids) {
        return jobControllerApi.deleteJobsUsingDELETE(ids);
    }
}