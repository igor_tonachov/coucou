import {TranslationControllerApi} from "../../../rest/api";
import {basePath} from "../constants/api";

const translateControllerApi = new TranslationControllerApi({basePath});

export class TranslateApiService {
    static translate(text) {
        return translateControllerApi.translateStringIntoSupportedLanguagesUsingGET(text);
    }
}