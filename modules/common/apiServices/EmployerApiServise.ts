import {EmployerControllerApi, EmployerCreation} from "../../../rest/api";
import {basePath} from "../constants/api";

const employerControllerApi = new EmployerControllerApi({basePath});


// window.t = employerControllerApi;

// for(var i=0; i<100; i++){
//     window.t.createEmployerUsingPOST({
//         companyName: 'COmpany name',
//         email: 'email@email.com',
//         phoneNumber: '78934234789'
//     });
// }

export class EmployerApiService {
    static getAll(page: number, size: number = 8) {
        return employerControllerApi.getAllEmployersUsingGET(page, size);
    }

    static getJobsByPage(employerId: number, page: number, size: number = 8) {
        return employerControllerApi.getJobsUsingGET(employerId, page, size);
    }

    static create(employer: EmployerCreation) {
        return employerControllerApi.createEmployerUsingPOST(employer);
    }

    static update(id: number, employer: EmployerCreation) {
        return employerControllerApi.updateEmployerUsingPATCH(id, employer);
    }

    static getById(id: number) {
        return employerControllerApi.getByIdUsingGET(id);
    }

    static removeAll(ids) {
        return employerControllerApi.deleteEmployersUsingDELETE(ids);
    }
}