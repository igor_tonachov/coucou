import {invertObj} from "ramda";

export enum PageNames{
    LOGIN = 'login',
    DASHBOARD = 'dashboard',
    EMPLOYER_UPDATE = 'employerUpdate',
    JOB_CREATE = 'jobCreate',
    JOB_UPDATE = 'jobUpdate',
    SKILL_CREATE = 'skillCreate',
    SKILL_UPDATE = 'skillUpdate',
    EDIT_REQUIREMENTS = "jobEditSkills"
}

export const routeAliases = {
    '/employer/:employerId/job/create': PageNames.JOB_CREATE,
    '/employer/:employerId/job/:jobId': PageNames.JOB_UPDATE,
    '/employer/:employerId/job/:jobId/requirements': PageNames.EDIT_REQUIREMENTS,
    '/skill/create': PageNames.SKILL_CREATE,
    '/skill/:skillId': PageNames.SKILL_UPDATE,
    '/employer/:employerId': PageNames.EMPLOYER_UPDATE,
    '/login': PageNames.LOGIN,
    "/": PageNames.DASHBOARD
};

export const pageNameAliases = invertObj(routeAliases);

export const authPageNames: string[] = [PageNames.LOGIN];