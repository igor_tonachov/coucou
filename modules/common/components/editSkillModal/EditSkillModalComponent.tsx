import * as React from "react";
import {Skill} from "@Interfaces";
import {Modal, Button, Typography, TextField, Select, MenuItem} from "material-ui";
import "./editSkillModal.scss";
import {FileUploader} from "@UI";
import {Languages, languagesList, TranslateApiService} from "@Common";

export class EditSkillModal extends React.Component<Props, State> {

    state = {
        id: '',
        title: [],
        description: [],
        imageUrl: '',
        isCreating: false,
        file: null,
        language: Languages.ENGLISH,
        languages: languagesList,
        showTranslateButton: false,
        imageErrorMessage: '',
        submitted: false,
        disableTranslateButton: false
    };

    componentWillMount() {
        const {skill} = this.props;

        if (skill) {
            this.setState({
                id: skill.id,
                title: skill.title,
                description: skill.description,
                imageUrl: skill.imageUrl
            })
        }
        else {
            this.setState({
                isCreating: true,
                title: [{languageCode: Languages.ENGLISH, content: ''}],
                description: [{languageCode: Languages.ENGLISH, content: ''}],
            });
        }
    }

    titleChanged = value => {
        const titleToChange = this.state.title.find(title => title.languageCode == this.state.language);
        const index = this.state.title.indexOf(titleToChange);
        const {title} = this.state;
        title[index].content = value;
        this.setState({title: [...title]});
    };

    descriptionChanged = value => {
        const descriptionToChange = this.state.description.find(description => description.languageCode == this.state.language);
        const index = this.state.description.indexOf(descriptionToChange);
        const {description} = this.state;
        description[index].content = value;
        this.setState({description: [...description]});
    };

    getImageErrorMessage = (file) => {
        return file ? '' : 'select skill image';
    };

    fileAdded = (file) => {
        this.setState({file, imageErrorMessage: this.getImageErrorMessage(file)})
    };

    handleLanguageChange = (e) => {
        const {title, description} = this.state;
        const newLanguage = e.target.value;

        if (!title.find(item => item.languageCode == newLanguage)) {
            this.setState({
                title: [...title, {languageCode: newLanguage, content: ''}]
            })
        }

        if (!description.find(item => item.languageCode == newLanguage)) {
            this.setState({
                description: [...description, {languageCode: newLanguage, content: ''}]
            })
        }

        this.setState({language: newLanguage});
    };

    translate = () => {
        const {title, description} = this.state;
        const englishTitle = title.find(item => item.languageCode == Languages.ENGLISH);
        const englishDescription = description.find(item => item.languageCode == Languages.ENGLISH);

        this.setState({disableTranslateButton: true});

        const promise1 = TranslateApiService.translate(englishTitle.content).then(resp => {
            this.setState({title: [englishTitle, ...resp]});
        });

        const promise2 = TranslateApiService.translate(englishDescription.content).then(resp => {
            this.setState({description: [englishDescription, ...resp]});
        });

        Promise.all([promise1, promise2])
            .then(() => {
                this.setState({disableTranslateButton: false});
            })
            .catch(() => {
                this.setState({disableTranslateButton: false});
            });
    };

    validate = () => {
        const {isCreating} = this.state;

        const imageErrorMessage = isCreating && this.getImageErrorMessage(this.state.file) || '';
        this.setState({
            imageErrorMessage,
            submitted: true
        });

        return !imageErrorMessage;
    };

    create = () => {
        if (this.validate()) {
            this.props.createAction({
                description: this.state.description,
                title: this.state.title,
                image: this.state.file
            })
        }
    };

    update = () => {
        if (this.validate()) {
            this.props.updateAction({
                id: this.state.id,
                description: this.state.description,
                title: this.state.title,
                image: this.state.file
            });
        }
    };

    render() {
        const {cancelAction} = this.props;
        const {id, title, description, isCreating, languages, language, imageUrl, imageErrorMessage, disableTranslateButton} = this.state;

        const titleContentToChange = title.find(title => title.languageCode == language).content;
        const descriptionToChange = description.find(description => description.languageCode == language).content;
        const englishTitle = title.find(t => t.languageCode == Languages.ENGLISH);
        const englishDescription = description.find(t => t.languageCode == Languages.ENGLISH);


        const headerText = isCreating ? 'Add new skill' : 'Update skill';

        return (
            <Modal
                className="edit_skill_modal modal"
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={true}
                onClose={cancelAction}>
                <div>
                    <div className="edit_skill_modal_content">

                        <Typography type="title" className="title">
                            {headerText}
                        </Typography>

                        <div className="edit_skill_modal_translate">
                            <Select
                                value={language}
                                onChange={this.handleLanguageChange}>
                                {
                                    languages.map((language) => (
                                        <MenuItem value={language.key} key={language.key}>
                                            {language.text}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                            <If condition={language == Languages.ENGLISH}>
                                <Button
                                    size="small"
                                    onClick={this.translate}
                                    raised
                                    color="secondary"
                                    type="submit"
                                    disabled={disableTranslateButton || !englishTitle.content || !englishDescription.content}>
                                    Translate
                                </Button>
                            </If>
                        </div>

                        <If condition={!isCreating}>
                            <Typography>
                                {id}
                            </Typography>
                        </If>

                        <TextField
                            id="edit_skill_modal_title"
                            value={titleContentToChange}
                            label="Title"
                            onChange={evt => this.titleChanged(evt.target.value)}
                        />

                        <TextField
                            id="edit_skill_modal_description"
                            value={descriptionToChange}
                            label="Description"
                            multiline={true}
                            rowsMax={20}
                            onChange={evt => this.descriptionChanged(evt.target.value)}
                        />

                        <If condition={imageErrorMessage}>
                            <h4 className="error weight_medium">{imageErrorMessage}</h4>
                        </If>

                        <FileUploader fileAdded={this.fileAdded} image={imageUrl}/>
                    </div>

                    <div className="edit_skill_modal_actions">
                        <Choose>
                            <When condition={isCreating}>
                                <Button
                                    onClick={this.create}
                                    raised
                                    color="secondary"
                                    type="submit">
                                    Create
                                </Button>
                            </When>
                            <Otherwise>
                                <Button
                                    onClick={this.update}
                                    raised
                                    color="secondary"
                                    type="submit">
                                    Update
                                </Button>
                            </Otherwise>
                        </Choose>

                        <Button
                            raised
                            color="primary"
                            type="submit"
                            onClick={cancelAction}>
                            Cancel
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

interface Props {
    skill?: Skill;
    cancelAction: any;
    updateAction?: any;
    createAction?: any;
}

interface State {
    id: string;
    title: { languageCode: string; content: string; }[];
    description: { languageCode: string; content: string; }[];
    imageUrl: string;
    isCreating: boolean;
    file: any;
    language: string;
    languages: any[];
    showTranslateButton: boolean;
    imageErrorMessage: string;
    submitted: boolean;
    disableTranslateButton: boolean;
}