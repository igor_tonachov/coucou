import * as React from "react";
import {getDndContext} from "../services/dragAndDropContext";
import * as PropTypes from "prop-types";

export function dragAndDropContext(): any {
    return function (Child): any {

        class DNDContext extends React.Component<any, any> {

            static childContextTypes = {
                dragDropManager: PropTypes.object.isRequired
            };

            getChildContext() {
                return {
                    dragDropManager: getDndContext()
                };
            }

            render() {
                return <Child {...this.props} />
            }
        }

        return DNDContext;
    }
}
