export enum Languages{
    ENGLISH = 'en',
    GERMAN = 'de',
    FRENCH = 'fr',
    SPANISH = 'es',
    // PERSIAN = 'fa',
    // ARABIC = 'ar'
}

export const languagesObj = {
    [Languages.ENGLISH]: 'English',
    [Languages.GERMAN]: 'German',
    [Languages.FRENCH]: 'French',
    [Languages.SPANISH]: 'Spanish',
    // [Languages.PERSIAN]: 'Persian',
    // [Languages.ARABIC]: 'Arabic',
};


export const languagesList = Object.keys(languagesObj)
    .map((languageKey) => ({
        key: languageKey,
        text: languagesObj[languageKey]
    }));