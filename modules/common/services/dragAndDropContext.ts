import {DragDropManager} from 'dnd-core';
import HTML5Backend from 'react-dnd-html5-backend';
import {always} from "ramda";

export const getDndContext: any = always(new DragDropManager(HTML5Backend));

