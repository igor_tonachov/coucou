export const getUserLocale = (availableLocales: { name: string; }[]) => {
    const locale = availableLocales.find(locale => locale.name == navigator.language.substring(0, 2));
    return locale ? locale.name : 'en';
};

export const availableLocales = [
    {icon: 'lang_rus', name: 'ru', text: 'Русский'},
    {icon: 'lang_eng', name: 'en', text: 'English'}
];