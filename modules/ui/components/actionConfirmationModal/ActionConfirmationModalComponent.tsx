import * as React from "react";
import {Typography, Button, Modal} from "material-ui";
import * as cx from "classnames";
import "./actionConfirmationModal.scss";

export class ActionConfirmationModal extends React.Component <Props, {}> {
    render() {
        const {title, okAction, noAction, className, yesText, noText} = this.props;

        const classes = cx('action_confirmation_modal modal', className);

        return (
            <Modal
                className={classes}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={true}
                onClose={noAction}>
                <div>
                    <Typography className="action_confirmation_modal_title">
                        {title}
                    </Typography>
                    <div className="action_confirmation_modal_actions">
                        <Button
                            color="secondary"
                            type="submit"
                            onClick={noAction}>
                            {noText || 'No'}
                        </Button>
                        <Button onClick={okAction}
                                color="secondary"
                                type="submit">
                            {yesText || 'Yes'}
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

interface Props {
    title: string;
    okAction: any;
    noAction: any;
    yesText?: string;
    noText?: string;
    className?: string;
}