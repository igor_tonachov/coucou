import * as React from "react";
import "./fileUploader.scss";
import Dropzone from 'react-dropzone';

export class FileUploader extends React.Component <Props, State> {

    state = {
        filePreview: null
    };

    onDrop = (acceptedFiles) => {
        const file = acceptedFiles && acceptedFiles[0];

        if (file) {
            var reader = new FileReader();
            reader.onload = (e: any) => {
                this.setState({filePreview: e.target.result});
                this.props.fileAdded(e.target.result);
            };
            reader.readAsDataURL(acceptedFiles[0]);
        }
    };

    render() {

        const {image, highlighted} = this.props;
        const {filePreview} = this.state;
        return (
            <div className={(image && highlighted) ? "file_uploader file-selected" : "file_uploader"}>
                <If condition={filePreview || image}>
                    <img className="preview_image" src={image || filePreview}/>
                </If>
                <Dropzone
                    className="file_uploader_zone"
                    disableClick={true}
                    multiple={false}
                    onDrop={this.onDrop}
                    disablePreview={false}
                />
            </div>
        );
    }
}

interface Props {
    image?: string;
    highlighted?: boolean;
    fileAdded: Function;
}

interface State {
    filePreview: any;
}