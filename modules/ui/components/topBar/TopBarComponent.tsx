import * as React from "react";
import {AppBar, Typography, Toolbar} from "material-ui";
import "./topBar.scss";
import {connect} from "react-redux";
import {PageNames} from "@Common";
import {ArrowBack} from 'material-ui-icons';
import {withRouter} from "react-router";

const con: any = connect;
const withR: any = withRouter;

@con(store => ({
    activePageName: store.router && store.router.activePageName,
    params: store.router && store.router.params
}))
@withR
export class TopBar extends React.Component <Props, {}> {

    toDashboard = () => {
        this.props.history.push('/')
    };

    toEmployer = () => {
        const {params, history} = this.props;
        history.push(`/employer/${params.employerId}`);
    };

    render() {
        const {activePageName} = this.props;

        return (
            <AppBar className="top_bar">
                <Toolbar>
                    <div>
                        <img src="/static/images/logo_coucou.png"/>
                        <Typography type="display1">
                            Coucou
                        </Typography>
                    </div>

                    <Choose>
                        <When condition={activePageName == PageNames.EMPLOYER_UPDATE}>
                            <div className="to_dashboard" onClick={this.toDashboard}>
                                <ArrowBack/>
                                <h5 className="weight_medium">Back to Dashboard</h5>
                            </div>
                        </When>
                        <When
                            condition={activePageName == PageNames.JOB_UPDATE
                            || activePageName == PageNames.JOB_CREATE
                            || activePageName == PageNames.EDIT_REQUIREMENTS}>
                            <div className="to_dashboard" onClick={this.toEmployer}>
                                <ArrowBack/>
                                <h5 className="weight_medium">To Employer</h5>
                            </div>
                        </When>
                    </Choose>
                </Toolbar>
            </AppBar>
        );
    }
}

interface Props {
    history?: any;
    params?: any;
    activePageName?: PageNames;
}