import * as React from "react";
import "./paginator.scss";
import * as ReactPaginate from 'react-paginate';
import {Meta} from "@Interfaces";

export class Paginator extends React.Component <Props, {}> {
    render() {
        const {meta, onPageChange} = this.props;

        return (
            <If condition={meta}>
                <div className="paginator">
                    <ReactPaginate
                        pageCount={meta.totalPages}
                        pageRangeDisplayed={2}
                        marginPagesDisplayed={2}
                        forcePage={meta.number}
                        onPageChange={onPageChange}
                    />
                </div>
            </If>
        );
    }
}

interface Props {
    meta: Meta;
    onPageChange: Function;
}