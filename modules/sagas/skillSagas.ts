import {put, takeEvery, select} from "redux-saga/effects";
import {
    getActivePageName, getSkillsMeta, getSkillsSelector, mainActionTypes, routerActionTypes, jobActionTypes,
    skillsActionTypes, showSkillStep, searchSkills, getSkillDetails, deleteParentSkillSuccess, deleteSubSkillSuccess
} from "@StoreCommon";
import {PageNames, SkillsApiService, JobApiService} from "@Common";
import {searchResult} from "@Interfaces";
import {reactOnExceptions} from "./mainSagas";
import {NotificationsService, NavigationService} from "@Sagas";

function* fetchSkillDataByPage() {
    const router = yield select((store: any) => store.router);
    const pageNumber = router.parsedSearchParams.skillsPage || 0;
    const query = router.parsedSearchParams.skillSearch || '';
    let resp = null;
    if(!query) {
        resp = yield SkillsApiService.getAll(pageNumber);
    }else {
        resp = yield replaceSkillsBySearched({payload:{query, excludeSkillIds: [], page: 0, size: 4}});
    }

    const skills = resp.content;
    const meta = resp;
    delete meta.content;

    yield put({
        type: skillsActionTypes.GET_SKILLS_PER_PAGE_SUCCESS,
        payload: {
            skills,
            meta
        }
    });
}

function* fetchSkillDataIfNeeded(action) {
    const activePageName = yield select(getActivePageName);
    const skillsMeta = yield select(getSkillsMeta);
    const skills = yield  select(getSkillsSelector);
    try {
        if (!skillsMeta.fetched) {
            switch (activePageName) {
                case PageNames.DASHBOARD: {
                    yield fetchSkillDataByPage();
                    break;
                }
                case PageNames.SKILL_UPDATE: {
                    if (Object.keys(skills).length > 0) break;
                    yield fetchSkillDataByPage();
                    break;
                }
                case PageNames.JOB_UPDATE: {
                    const router = yield select((state: any) => state.router);
                    let jobToUpdate = yield select((state: any) => state.jobs[router.params.jobId]);
                    if (!jobToUpdate) {
                        const job = yield JobApiService.getById(router.params.jobId);
                        delete job.employer;
                        job.employerId = router.params.employerId;
                        yield put({type: jobActionTypes.GET_JOB_BY_ID_SUCCESS, payload: {job}});
                    }
                    break;
                }
                case PageNames.EDIT_REQUIREMENTS: {
                    yield fetchSkillDataByPage();
                    break;
                }
            }
        }
    } catch (exc) {
        yield reactOnExceptions(exc);
    }
}

export function* addSkill(action) {
    try {
        let {payload} = action;
        const {query} = payload;
        delete payload.query;
        const router = yield select((store: any) => store.router);
        const resp = yield SkillsApiService.create(payload);
        NotificationsService.addNotification(`Skill was added`, 'success');
        yield put({type: skillsActionTypes.ADD_SKILL_SUCCESS, payload: {skill: resp.skill, currentSkillId: resp.skill.id}});
        const skillsMeta = yield select((store: any) => store.meta.skills);
        const page = yield select((store: any) => store.skills.skillPage);
        const skillPage = page + 1;

        yield put(showSkillStep({skillPage}) );
        yield put(searchSkills({query}) );
        yield put({
            type: mainActionTypes.CHANGE_MULTIPLE_QUERY_STRINGS,
            payload: {
                params: [
                    {
                        parameterName: 'skillsPage',
                        value: skillsMeta.totalPages - 1
                    }
                ]
            }
        });

        return resp.skill;
    }catch (exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Skill was not added`, 'error');
    }
}

function* removeSkills(action) {
    try {
        yield SkillsApiService.removeAll(action.payload.ids);
        NotificationsService.addNotification(`Skill was removed`, 'success');
    }catch (exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Skill was not removed`, 'error');
    }
}

function* addSubSkills(action) {
    try {
        const {id, subSkillId} = action.payload;
        yield SkillsApiService.addSubSkill(id, subSkillId);
        yield put(getSkillDetails({id}));
        NotificationsService.addNotification(`Skill was added to sub-skills`, 'success');
    }catch (exc) {
        const err = yield exc.json();
        const {code, message} = err.error;
        if(code === "SkillExists") {
            NotificationsService.addNotification(`${message}`, 'error');
            return;
        }
        NotificationsService.addNotification(`Skill was not added to parent`, 'error');
    }
}

function* addParentSkills(action) {
    try {
        const {id, parentSkillId} = action.payload;
        yield SkillsApiService.addParentSkill(id, parentSkillId);
        yield put(getSkillDetails({id}));
        NotificationsService.addNotification(`Skill was added to parent`, 'success');
    }catch (exc) {
        const err = yield exc.json();
        const {code, message} = err.error;
        if(code === "SkillExists") {
            NotificationsService.addNotification(`Skill won't be added to prevent circular dependencies`, 'error');
            return;
        }
        NotificationsService.addNotification(`Skill was not added to parent`, 'error');
    }
}

function* getSkillsBySearch(action) {
    try {
        const resp: searchResult  = yield SkillsApiService.getSkillsBySearch(action.payload.query, action.payload.excludeSkillIds, 0, 4);
        yield put({
            type: skillsActionTypes.SEARCH_SKILLS_SUCCESS,
            payload: {
                searchResults: resp.content
            }
        });
    }catch(exc) {
        yield reactOnExceptions(exc);
    }
}

function* replaceSkillsBySearched(action) {
    try {
        const resp: searchResult  = yield SkillsApiService.getSkillsBySearch(action.payload.query, action.payload.excludeSkillIds, 0, 0);
            yield put({
                type: skillsActionTypes.SEARCH_SKILLS_SUCCESS,
                payload: {
                    skillsList: resp.content
                }
            });
    }catch(exc) {
        yield reactOnExceptions(exc);
    }
}

function* updateSkill(action) {
    try {
        const {payload} = action;
        let skill = {...payload};
        const updatedSkill = yield SkillsApiService.update(skill.id, skill);
        NotificationsService.addNotification(`Skill was updated`, 'success');
        yield put({type: skillsActionTypes.UPDATE_SKILL_SUCCESS, payload: {skill: updatedSkill}});
        if(payload.backRouteData) {
            const {employerId, jobId, path, jobTitle} = payload.backRouteData;
            if(path && path.length) {
                yield put({type: jobActionTypes.ADD_SUB_REQUIREMENT, payload: {jobId, requirementId:path[path.length-1], id:skill.id, path} });
            }else {
                yield put({type: jobActionTypes.ADD_REQUIREMENT, payload: {jobId, id:skill.id, path} });
            }
            NavigationService.navigate({pathname:`/employer/${employerId}/job/${jobId}/requirements`, state:{path, jobTitle}});
        }else {
            NavigationService.navigate('/');
        }
    }catch (exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Skill was not updated`, 'error');
    }
}

function* deleteParentSkills(action) {
    try {
        const {payload} = action;
        yield SkillsApiService.removeParentSkill(payload.id, payload.parentSkillId);
        yield put(deleteParentSkillSuccess({parentSkillId:payload.parentSkillId}) );
        NotificationsService.addNotification(`Skill was removed`, 'success');
    }catch (exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Skill was not removed`, 'error');
    }
}

function* deleteSubSkills(action) {
    try {
        const {payload} = action;
        yield SkillsApiService.removeSubSkill(payload.id, payload.subSkillId);
        yield put(deleteSubSkillSuccess({subSkillId:payload.subSkillId}) );
        NotificationsService.addNotification(`Skill was removed`, 'success');
    }catch (exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Skill was not removed`, 'error');
    }
}

function* getDetails(action) {
    try {
        const {payload} = action;
        const details = yield SkillsApiService.getSkillDetails(payload.id);
        yield put({type: skillsActionTypes.GET_SKILL_DETAILS_SUCCESS, payload:{...details} });
    }catch (exc) {
        yield reactOnExceptions(exc);
    }
}

const skillSagas = [
    takeEvery(routerActionTypes.LOCATION_CHANGE, fetchSkillDataIfNeeded),
    takeEvery(skillsActionTypes.ADD_SKILL, addSkill),
    takeEvery(skillsActionTypes.REMOVE_SKILLS, removeSkills),
    takeEvery(skillsActionTypes.UPDATE_SKILL, updateSkill),
    takeEvery(skillsActionTypes.SEARCH_SKILLS, getSkillsBySearch),
    takeEvery(skillsActionTypes.SAVE_PARENT_SKILL, addParentSkills),
    takeEvery(skillsActionTypes.SAVE_SUB_SKILL, addSubSkills),
    takeEvery(skillsActionTypes.DELETE_PARENT_SKILL, deleteParentSkills),
    takeEvery(skillsActionTypes.DELETE_SUB_SKILL, deleteSubSkills),
    takeEvery(skillsActionTypes.GET_SKILL_DETAILS, getDetails)
];

export {skillSagas};