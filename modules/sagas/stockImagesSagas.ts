import {put, takeEvery, select} from "redux-saga/effects";
import {imagesActionTypes, stockImageRequest, stockImageFetched} from "@StoreCommon";
import {ImageApiService} from "@Common";
import {reactOnExceptions} from "./mainSagas";
import {NotificationsService} from "@Sagas";


function* fetchStockImages(action) {
    const query = action.payload;
    try {
        yield put(stockImageRequest({pending: true, fetched: false}) );
        const resp = yield ImageApiService.getStockImages(query);
        yield put(stockImageFetched({pending: false, fetched: true}) );
        yield put({
            type: imagesActionTypes.FETCH_STOCK_IMAGES_SUCCESS,
            payload: resp

        })
    }catch(exc) {
        yield reactOnExceptions(exc);
        NotificationsService.addNotification(`Can't load images`, 'error');
        yield put(stockImageFetched({pending: false, fetched: false}) );
    }

}

const stockImagesSagas = [
    takeEvery(imagesActionTypes.FETCH_STOCK_IMAGES, fetchStockImages),
];

export {stockImagesSagas};