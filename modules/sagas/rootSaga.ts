import {localeSagas} from "./localeSagas";
import {skillSagas} from "./skillSagas";
import {employerSagas} from "./employerSagas";
import {jobSagas} from "./jobSagas";
import {mainSagas} from "./mainSagas";
import {jobPageSagas} from "./jobPageSagas";
import {stockImagesSagas} from "./stockImagesSagas";

export function* rootSaga() {
    yield [
        ...localeSagas,
        ...skillSagas,
        ...employerSagas,
        ...jobSagas,
        ...mainSagas,
        ...jobPageSagas,
        ...stockImagesSagas
    ];
}