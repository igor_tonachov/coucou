import {put, takeEvery, select} from "redux-saga/effects";

import {EmployerApiService, JobApiService} from "@Common";
import {mainActionTypes, jobActionTypes} from "@StoreCommon";
import {NavigationService} from "@Sagas";

import {reactOnExceptions} from "./mainSagas";
import {NotificationsService} from "./services/NotificationsService";
import {addSkill} from "./skillSagas";

function* fetchEmployerListOfJobs() {
    try {
        const router = yield select((state: any) => state.router);
        const page = router.parsedSearchParams.jobPage || 0;
        const resp = yield EmployerApiService.getJobsByPage(router.params.employerId, page, 20);
        const jobs = resp.content.map(job => {
            delete job.employer;
            job.employerId = router.params.employerId;
            return job;
        });
        let meta = resp;
        delete meta.content;
        const jobId = router.params.jobId;
        yield put({type: jobActionTypes.GET_EMPLOYER_JOB_PER_PAGE_SUCCESS, payload: {jobs, meta, jobId:jobId}});
    } catch (e) {
        yield reactOnExceptions(e);
    }
}

function* removeJobs(action) {
    try {
        yield JobApiService.removeAll(action.payload.ids);
        yield put({type: jobActionTypes.REMOVE_JOBS_SUCCESS, payload: {ids:action.payload.ids}});
        NotificationsService.addNotification(`Job was removed`, 'success');
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Job was not removed`, 'error');
    }
}

function* addJob(action) {
    try {
        const newJob = yield JobApiService.create(action.payload);
        NotificationsService.addNotification(`New Job was added`, 'success');
        yield put({type: jobActionTypes.ADD_JOB_SUCCESS, payload: {newJob}});
        NavigationService.navigate(`/employer/${action.payload.employerId}/job/${newJob.id}/requirements`);
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`New Job was not added`, 'error');
    }
}

function* updateJob(action) {
    try {
        yield JobApiService.update(action.payload.id, action.payload);
        yield put({type: jobActionTypes.UPDATE_JOB_SUCCESS, payload: action.payload});
        NotificationsService.addNotification(`Job was updated`, 'success');
        NavigationService.navigate(`/employer/${action.payload.employerId}/job/${action.payload.id}/requirements`);
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Job was not updated`, 'error');
    }
}

function* removeSelectedSkillFromJob(action) {
    try {
        yield JobApiService.removeSkill(action.payload.jobId, action.payload.requirementId);
        yield put({type: jobActionTypes.REMOVE_REQUIREMENT_SUCCESS, payload: {
            jobId:action.payload.jobId,
            requirementId:action.payload.requirementId,
            path:action.payload.path
            }
        });
        NotificationsService.addNotification(`Requirement was removed`, 'success');
    } catch (e) {
        const err = yield e.json();
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Requirement was not removed`, 'error');
    }
}

function* addRequirementToJob(action) {
    try {
        const requirement = yield JobApiService.addSkill(action.payload.jobId, action.payload.id);
        yield put({type: jobActionTypes.ADD_REQUIREMENT_SUCCESS, payload: {jobId:action.payload.jobId, path:action.payload.path, requirement}});
        NotificationsService.addNotification(`Requirement was added`, 'success');
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Selected requirement was not changed`, 'error');
    }
}

function* addSubRequirement(action) {
    try {
        const requirement = yield JobApiService.addSubSkill(action.payload.jobId, action.payload.requirementId, action.payload.id);
        yield put({type: jobActionTypes.ADD_REQUIREMENT_SUCCESS, payload: {jobId:action.payload.jobId, path:action.payload.path, requirement}});
        NotificationsService.addNotification(`Requirement was added`, 'success');
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Selected requirement was not changed`, 'error');
    }
}

function* updateRequirement(action) {
    try {

        yield put({type: jobActionTypes.UPDATE_REQUIREMENT_SUCCESS, payload: {...action.payload} });
        NotificationsService.addNotification(`Requirement was updated`, 'success');
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Selected requirement was not changed`, 'error');
    }
}

const jobSagas = [
    takeEvery(jobActionTypes.GET_EMPLOYER_JOB_PER_PAGE, fetchEmployerListOfJobs),
    takeEvery(jobActionTypes.UPDATE_JOB, updateJob),
    takeEvery(jobActionTypes.ADD_JOB, addJob),
    takeEvery(jobActionTypes.REMOVE_JOBS, removeJobs),
    takeEvery(jobActionTypes.REMOVE_REQUIREMENT, removeSelectedSkillFromJob),
    takeEvery(jobActionTypes.ADD_REQUIREMENT, addRequirementToJob),
    takeEvery(jobActionTypes.ADD_SUB_REQUIREMENT, addSubRequirement),
    takeEvery(jobActionTypes.UPDATE_REQUIREMENT, updateRequirement),
];

export {jobSagas};