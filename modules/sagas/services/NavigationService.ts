export class NavigationService {

    static cb;

    static subscribe(cb) {
        this.cb = cb;
        return () => {
            this.cb = null
        }
    }

    static navigate(to) {
        this.cb && this.cb(to);
    }
}