type NotificationTypes = 'success' | 'error' | 'info';


export class NotificationsService {

    static cb: Function;

    static subscribe(cb: Function) {
        this.cb = cb;

        return () => {
            this.cb = null;
        }
    }

    static addNotification(text, type: NotificationTypes) {
        this.cb && this.cb(text, type);
    }
}