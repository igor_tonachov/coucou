import {put, takeEvery, select} from "redux-saga/effects";
import {jobPageActionTypes, mainActionTypes} from "@StoreCommon";

function* searchChanged(action) {

    const skillSearch = select((store: any) => store.router && store.router.parsedSearchParams && store.router.parsedSearchParams);

    if (skillSearch != action.payload.search) {
        yield put({
            type: mainActionTypes.CHANGE_MULTIPLE_QUERY_STRINGS,
            payload: {
                params: [
                    {
                        parameterName: 'skillSearch',
                        value: action.payload.search || ''
                    },
                    {
                        parameterName: 'skillsPage',
                        value: 0
                    }
                ]
            }
        })
    }
}

const jobPageSagas = [
    takeEvery(jobPageActionTypes.SEARCH_CHANGED, searchChanged)
];

export {jobPageSagas};