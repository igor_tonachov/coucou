import {put, takeEvery, select} from "redux-saga/effects";
import {employersActionTypes, getActivePageName, mainActionTypes, routerActionTypes} from "@StoreCommon";
import {PageNames, EmployerApiService} from "@Common";
import {reactOnExceptions} from "./mainSagas";
import {NotificationsService} from "@Sagas";
import {Employer} from "@Interfaces";
import {translate} from 'counterpart';

function* fetchSkillDataIfNeeded(action) {
    const activePageName = yield select(getActivePageName);

    try {
        switch (activePageName) {
            case PageNames.DASHBOARD: {
                const router = yield select((state: any) => state.router);
                const page = router.parsedSearchParams.employersPage || 0;
                const resp = yield EmployerApiService.getAll(page);
                const employers = resp.content;
                let meta = resp;
                delete meta.content;
                yield put({
                    type: employersActionTypes.GET_EMPLOYERS_PER_PAGE_SUCCESS,
                    payload: {employers, meta}
                });
                break;
            }
            case PageNames.EMPLOYER_UPDATE: {
                const router = yield select((store: any) => store.router);
                const employerToUpdate = yield select((store: any) => store.employers[router.params.employerId]);

                if (!employerToUpdate) {
                    const employer = yield EmployerApiService.getById(router.params.employerId);
                    yield put({type: employersActionTypes.GET_SINGLE_EMPLOYER_SUCCESS, payload: employer});
                }

                break;
            }
        }
    } catch (e) {
        yield reactOnExceptions(e);
    }
}

function* removeEmployers(action) {
    try {
        yield EmployerApiService.removeAll(action.payload.ids);
        NotificationsService.addNotification(translate("notifications.employerRemoved.success"), 'success');
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(translate("notifications.employerRemoved.error"), 'error');
    }
}

function* addEmployer(action) {
    try {
        const newEmployer: Employer = yield EmployerApiService.create(action.payload.newEmployer);
        NotificationsService.addNotification(`New employer ${newEmployer.companyName} created`, 'success');
        yield put({type: employersActionTypes.ADD_EMPLOYER_SUCCESS, payload: {newEmployer}});
        const employersMeta = yield select((store: any) => store.meta.employers);

        yield put({
            type: mainActionTypes.CHANGE_MULTIPLE_QUERY_STRINGS,
            payload: {
                params: [
                    {
                        parameterName: 'employersPage',
                        value: employersMeta.totalPages - 1
                    }
                ]
            }
        })

    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`New employer was not created`, 'error');
    }
}

function* updateEmployer(action) {
    try {
        yield EmployerApiService.update(action.payload.id, action.payload);
        NotificationsService.addNotification(`Employer was updated`, 'success');
        yield put({type: employersActionTypes.UPDATE_EMPLOYER_SUCCESS, payload: action.payload});
    } catch (e) {
        yield reactOnExceptions(e);
        NotificationsService.addNotification(`Employer was not updated`, 'error');
    }
}

const employerSagas = [
    takeEvery(routerActionTypes.LOCATION_CHANGE, fetchSkillDataIfNeeded),
    takeEvery(employersActionTypes.ADD_EMPLOYER, addEmployer),
    takeEvery(employersActionTypes.REMOVE_EMPLOYERS, removeEmployers),
    takeEvery(employersActionTypes.UPDATE_EMPLOYER, updateEmployer)
];

export {employerSagas};