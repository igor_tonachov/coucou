import {put, takeEvery} from "redux-saga/effects";
import axios from 'axios';
import * as counterpart from 'counterpart';
import {mainActionTypes} from "@StoreCommon";

function* changeLocale(action) {
    const {localeName} = action.payload;

    const resp = yield axios.get(`/static/locale/${localeName}.json`);
    yield counterpart.registerTranslations(localeName, resp.data);
    yield counterpart.setLocale(localeName);
    yield put({type: mainActionTypes.CHANGE_LOCALE_SUCCESS, action});
}

const localeSagas = [
    takeEvery(mainActionTypes.CHANGE_LOCALE, changeLocale)
];

export {localeSagas};