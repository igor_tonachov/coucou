import {put, takeEvery, select} from "redux-saga/effects";
import {mainActionTypes, routerActionTypes} from "@StoreCommon";
import {NavigationService} from "@Sagas";

export function* reactOnExceptions(resp) {
    yield redirectIfUnauthorized(resp);
}

export function* redirectIfUnauthorized(resp) {
    if (resp.status == 401) {
        const login = yield select((store: any) => store.main && store.main.login);
        if (login) {
            yield put({type: mainActionTypes.USER_LOGOUT, payload: {}});
            yield put({type: routerActionTypes.LOCATION_CHANGE, payload: {pathname: '/login'}});
        }
    }
}

function* userLogin(action) {

    // const resp = yield

    // console.log(resp.error, resp.message)
    //
    // if (resp.error) {
    //
    //
    //     yield put({type: mainActionTypes.USER_LOGGIN_ERROR, payload: {message: resp.message}});
    // }
    // else {
    //     console.log(resp)
    // }
}

function* changeMultipleQueryStrings(action) {
    const {params} = action.payload;
    const router = yield select((state: any) => state.router);
    const parameters = Object.keys(router.parsedSearchParams);

    params.forEach(item => {
        if (parameters.indexOf(item.parameterName) == -1) {
            parameters.push(item.parameterName);
        }

        if (item.value == 0) {
            const index = parameters.indexOf(item.parameterName);
            parameters.splice(index, 1);
        }
    });

    let queryString = parameters.reduce((currentQueryString, param) => {
        if (currentQueryString) {
            currentQueryString = `${currentQueryString}&`;
        }

        const paramItem = params.find(p => p.parameterName == param);
        currentQueryString = `${currentQueryString}${param}=${paramItem ? paramItem.value : router.parsedSearchParams[param]}`;
        return currentQueryString;
    }, '');

    NavigationService.navigate(`${window.location.pathname}?${queryString}`);
}

const mainSagas = [
    takeEvery(mainActionTypes.USER_LOGIN, userLogin),
    takeEvery(mainActionTypes.CHANGE_MULTIPLE_QUERY_STRINGS, changeMultipleQueryStrings)
];

export {mainSagas};