import {routerReducer, LOCATION_CHANGE} from 'react-router-redux';
import * as UrlPattern from 'url-pattern';
import {routeAliases} from "@Common";
import {assocPath, invertObj} from "ramda";
import {parse, toObject} from "search-params";

export const router = (state, action) => {
    let newState = routerReducer(state, action);

    if (action.type === LOCATION_CHANGE) {
        const keys = Object.keys(routeAliases);

        for (let i = 0; i < keys.length; i++) {
            let pattern = keys[i];
            const result = (new UrlPattern(pattern)).match(action.payload.pathname);

            if (result) {
                const searchParams = window.location.search.substring(1, window.location.search.length);
                const parsedSearchParams = searchParams? toObject(parse(searchParams)):{};

                newState = assocPath(['activePageName'], routeAliases[pattern], newState);
                newState = assocPath(['params'], result, newState);

                return assocPath(['parsedSearchParams'], parsedSearchParams, newState);
            }
        }
    }

    return newState;
};