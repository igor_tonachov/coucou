import {createAction} from "../services/createAction";
import {jobActionTypes} from "./jobsActionTypes";

export const updateJob = createAction(jobActionTypes.UPDATE_JOB);
export const addJob = createAction(jobActionTypes.ADD_JOB);
export const removeJobs = createAction(jobActionTypes.REMOVE_JOBS);
export const addRequirement = createAction(jobActionTypes.ADD_REQUIREMENT);
export const addSubRequirement = createAction(jobActionTypes.ADD_SUB_REQUIREMENT);
export const removeSelectedJobSkill = createAction(jobActionTypes.REMOVE_REQUIREMENT);
export const getEmployerJobsList = createAction(jobActionTypes.GET_EMPLOYER_JOB_PER_PAGE);
export const setCurrentRequirementsList = createAction(jobActionTypes.SET_CURRENT_REQUIREMENT_LIST);
export const updateRequirement = createAction(jobActionTypes.UPDATE_REQUIREMENT);

