import {assocPath, dissoc, assoc, insert, path, remove} from "ramda";
import {jobActionTypes} from "./jobsActionTypes";
import {addRequiremet, removeRequiremet, updateRequirementRating} from "@Utils";

export const jobs = (state, action) => {
    const {payload} = action;

    switch (action.type) {
        case jobActionTypes.GET_JOB_BY_ID_SUCCESS:
            return {...state, jobsList:{...state.jobsList, [payload.job.id]: payload.job}};

        case jobActionTypes.REMOVE_JOBS_SUCCESS:
            const jobs = payload.ids.reduce((currentState, id) => {
                return dissoc(id, currentState);
            }, state.jobsList);
            return {...state, jobsList:jobs};

        case jobActionTypes.UPDATE_JOB_SUCCESS: {
            const jobsList = assoc(payload.id, payload, state.jobsList);
            return {...state, jobsList, currentRequirements:jobsList[payload.id].requirements}
        }

        case jobActionTypes.ADD_JOB_SUCCESS:
            const jobsList = assoc(payload.newJob.id, payload.newJob, state.jobsList);
            return {...state, jobsList, currentRequirements:jobsList[payload.newJob.id].requirements};

        case jobActionTypes.GET_EMPLOYER_JOB_PER_PAGE_SUCCESS: {
            const jobsList =  payload.jobs.reduce((curr, job) => {
                return {...curr, ...{[job.id]: job}};
            }, {});
            const currentRequirements = payload.jobId ? jobsList[payload.jobId].requirements : [];
            return {...state, jobsList, currentRequirements};
        }

        case jobActionTypes.REMOVE_REQUIREMENT_SUCCESS: {
            removeRequiremet(state.jobsList[payload.jobId].requirements, payload.requirementId, payload.path);
            return {...state, jobsList:state.jobsList, currentRequirements:state.currentRequirements}
        }

        case jobActionTypes.ADD_REQUIREMENT_SUCCESS: {
            addRequiremet(state.jobsList[payload.jobId].requirements, payload.requirement, payload.path);
            return {...state, jobsList:state.jobsList}

        }

        case jobActionTypes.SET_CURRENT_REQUIREMENT_LIST: {
            const currentRequirements = payload.currentRequirements || [];
            return {...state, currentRequirements}
        }

        case jobActionTypes.UPDATE_REQUIREMENT_SUCCESS: {
            updateRequirementRating(state.jobsList[payload.jobId].requirements, payload.requirementId, payload.value, payload.path);
            return {...state, jobsList:state.jobsList}
        }

        default:
            return state || {};
    }
};