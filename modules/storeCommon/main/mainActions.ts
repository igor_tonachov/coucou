import {createAction} from "../services/createAction";
import {mainActionTypes} from "./mainActionTypes";

export const changeLocale = createAction(mainActionTypes.CHANGE_LOCALE);
export const setWindowDimensions = createAction(mainActionTypes.SET_WINDOW_DIMENSIONS);
export const changeLocaleSuccess = createAction(mainActionTypes.CHANGE_LOCALE_SUCCESS);
export const userLogin = createAction(mainActionTypes.USER_LOGIN);
export const changeQueryString = createAction(mainActionTypes.CHANGE_MULTIPLE_QUERY_STRINGS);