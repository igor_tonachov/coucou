import {mainActionTypes} from "./mainActionTypes";
import {assocPath} from "ramda";

export const main = (state, action) => {
    const {payload} = action;

    switch (action.type) {
        case mainActionTypes.CHANGE_LOCALE:
            return assocPath(['locale', 'name'], payload.localeName, state);
        case mainActionTypes.SET_WINDOW_DIMENSIONS:
            return {...state, ...payload};
        case mainActionTypes.CHANGE_LOCALE_SUCCESS:
            return assocPath(['locale', 'pending'], false, state);
        case mainActionTypes.USER_LOGIN:
            localStorage['login'] = true;
            return assocPath(['user'], {data: payload.user}, assocPath(['login'], true, state));
        case mainActionTypes.USER_LOGGIN_SUCCESS:
            return assocPath(['user'], payload);
        case mainActionTypes.USER_LOGOUT: {
            localStorage.removeItem('login');
            return assocPath(['user'], {data: null}, assocPath(['login'], false, state));
        }
        default:
            return state || {};
    }
};