import {assocPath, assoc} from "ramda";
import {imagesActionTypes} from "./imagesActionTypes";

export const stockImages = (state, action) => {
    const {payload} = action;

    switch (action.type) {
        case imagesActionTypes.FETCH_STOCK_IMAGES_SUCCESS:
            return payload;

        default:
            return state || {};
    }
};