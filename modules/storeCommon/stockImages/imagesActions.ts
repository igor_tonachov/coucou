import {createAction} from "../services/createAction";
import {imagesActionTypes} from "./imagesActionTypes";

export const fetchStockImages =createAction(imagesActionTypes.FETCH_STOCK_IMAGES);
