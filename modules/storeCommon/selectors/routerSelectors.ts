import {path} from "ramda";

export const getActivePageName = path(['router', 'activePageName']);