import {path} from "ramda";

export const getEmployersSelector = store => {
    const employers = path(['employers'], store);
    return Object.keys(employers).map(key => employers[key]);
};

export const getEmployerToEdit = store => {
    const employers = path(['employers'], store);
    const routeParams = path(['router', 'params'], store);
    return employers && routeParams.employerId && employers[routeParams.employerId];
};

export const getEmployerJobs = store => {
    let jobs = path(['jobs', 'jobsList'], store);
    jobs = Object.keys(jobs).map(key => jobs[key]);
    const routeParams = path(['router', 'params'], store);
    return jobs && routeParams.employerId && jobs.filter(job => job.employerId == routeParams.employerId);
};