import {path} from "ramda";

export const getSkillsMeta = path(['meta', 'skills']);
export const getEmployersMeta = path(['meta', 'employers']);
export const getJobsMeta = path(['meta', 'jobs']);