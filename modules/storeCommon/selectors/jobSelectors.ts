import {path} from "ramda";

export const getIsCreating = (store) => typeof path(['router', 'params', 'jobId'], store) == "undefined";

export const getJobToUpdate = store => {
    if (getIsCreating(store)) {
        return null;
    }
    const jobId = path(['router', 'params', 'jobId'], store);
    return store.jobs.jobsList[jobId];
};
