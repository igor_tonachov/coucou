import {path, values} from "ramda";
import {Languages} from "@Common";

export const getSkillsSelector = store => {
    const skills = path(['skills', 'skillsList'], store);
    return Object.keys(skills).map(key => skills[key]);
};

export const getSkillByIdSelector = (store) => {
    const skills = path(['skills'], store);
    if (!Object.keys(skills.skillsList).length) return null;
    const {skillId} = store.router.params;
    return values(skills.skillsList).find(skill => skill.id == skillId);
};

export const getSearchResultsSelector = (store) => {
    const skills = path(['skills'], store);
    if (!skills.searchResults.length) return [];
    let searchResults = [];
    for (let s in skills.searchResults) {
        const skill = skills.searchResults[s];
        if (skill.id !== skills.currentSkillId) {
            const title = skill.title.find((t) => t.languageCode === Languages.ENGLISH).content;
            const description = skill.description.find((t) => t.languageCode === Languages.ENGLISH).content;
            searchResults.push({id: skill.id, title, description})
        }
    }
    return searchResults;
};

export const getParentSkillsSelector = (store) => {
    const skills = path(['skills'], store);
    if (!skills.parentSkills.length) return [];
    let parentSkills = [];
    for (let s in skills.parentSkills) {
        const skill = skills.parentSkills[s];
        const title = skill.title.find((t) => t.languageCode === Languages.ENGLISH).content;
        const description = skill.description.find((t) => t.languageCode === Languages.ENGLISH).content;
        parentSkills.push({id: skill.id, title, description})
    }
    return parentSkills;
};

export const getSubSkillsSelector = (store) => {
    const skills = path(['skills'], store);
    if (!skills.subSkills.length) return [];
    let subSkills = [];
    for (let s in skills.subSkills) {
        const skill = skills.subSkills[s];
        const title = skill.title.find((t) => t.languageCode === Languages.ENGLISH).content;
        const description = skill.description.find((t) => t.languageCode === Languages.ENGLISH).content;
        subSkills.push({id: skill.id, title, description})
    }
    return subSkills;
};

export const getRelatedSkillsIdsSelector = (store) => {
    const skills = path(['skills'], store);
    if (!(skills.parentSkills && skills.subSkills)) return [];
    const parentSkillsIds = skills.parentSkills.map((skill) => skill.id);
    const subSkillsIds = skills.subSkills.map((skill) => skill.id);
    return [].concat(parentSkillsIds, subSkillsIds);
};
