import {createAction} from "../services/createAction";
import {jobPageActionTypes} from "./jobPageActionTypes";

export const startDrag = createAction(jobPageActionTypes.START_DRAG);
export const endDrag = createAction(jobPageActionTypes.END_DRAG);
export const searchChanged = createAction(jobPageActionTypes.SEARCH_CHANGED);