export enum jobPageActionTypes{
    START_DRAG = "JOB_PAGE :: START_DRAG",
    END_DRAG = "JOB_PAGE :: END_DRAG",
    SEARCH_CHANGED = "JOB_PAGE :: SEARCH_CHANGED"
}