import {assocPath, dissoc, assoc, insert, path, remove} from "ramda";
import {jobPageActionTypes} from "./jobPageActionTypes";

export const jobPage = (state, action) => {
    const {payload} = action;

    switch (action.type) {

        case jobPageActionTypes.START_DRAG: {
            return assoc('dragData', payload, state);
        }

        case jobPageActionTypes.END_DRAG: {
            return dissoc('dragData', state);
        }

        default:
            return state || {};
    }
};