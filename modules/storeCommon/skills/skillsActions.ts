import {createAction} from "../services/createAction";
import {skillsActionTypes} from "./skillsActionTypes";

export const updateSkill = createAction(skillsActionTypes.UPDATE_SKILL);
export const addSkill = createAction(skillsActionTypes.ADD_SKILL);
export const removeSkills = createAction(skillsActionTypes.REMOVE_SKILLS);
export const showSkillStep = createAction(skillsActionTypes.SHOW_SKILL_STEP);
export const setCurrentSkillId = createAction(skillsActionTypes.SET_CURRENT_SKILL_ID);
export const searchSkills = createAction(skillsActionTypes.SEARCH_SKILLS);
export const saveParentSkill = createAction(skillsActionTypes.SAVE_PARENT_SKILL);
export const saveSubSkill = createAction(skillsActionTypes.SAVE_SUB_SKILL);
export const deleteParentSkill = createAction(skillsActionTypes.DELETE_PARENT_SKILL);
export const deleteSubSkill = createAction(skillsActionTypes.DELETE_SUB_SKILL);
export const deleteParentSkillSuccess = createAction(skillsActionTypes.DELETE_PARENT_SKILL_SUCCESS);
export const deleteSubSkillSuccess = createAction(skillsActionTypes.DELETE_SUB_SKILL_SUCCESS);
export const getSkillDetails = createAction(skillsActionTypes.GET_SKILL_DETAILS);
