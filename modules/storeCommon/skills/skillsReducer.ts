import {assocPath, assoc, dissoc} from 'ramda';
import {skillsActionTypes} from "./skillsActionTypes";

export const skills = (state, action) => {
    const {payload} = action;

    switch (action.type) {
        case skillsActionTypes.GET_SKILLS_PER_PAGE_SUCCESS:
            let skillsList = payload.skills.reduce((curr, skill) => {
                return {...curr, ...{[skill.id]: skill}};
            }, {});
            return {...state, skillsList, skillPage:1, currentSkillId: null};

        case skillsActionTypes.REMOVE_SKILLS:
            return payload.ids.reduce((currentState, id) => {
                return dissoc(id, currentState);
            }, state);

        case skillsActionTypes.ADD_SKILL_SUCCESS:
            const currentSkillId = payload.currentSkillId;
            assoc(payload.skill.id, payload.skill, state);
            return {...state, currentSkillId};

        case skillsActionTypes.UPDATE_SKILL_SUCCESS:
            return assoc(payload.skill.id, payload.skill, state);

        case skillsActionTypes.SET_CURRENT_SKILL_ID:
            return {...state, ...payload};

        case skillsActionTypes.UPDATE_SKILL:
            return assoc(payload.id, payload, state);

        case skillsActionTypes.SHOW_SKILL_STEP:
            return {...state, ...payload};

        case skillsActionTypes.SEARCH_SKILLS_SUCCESS:
            return {...state, ...payload};

        case skillsActionTypes.GET_SKILL_DETAILS_SUCCESS:
            return {...state, parentSkills:payload.parentSkills,  subSkills:payload.subSkills};

        case skillsActionTypes.DELETE_PARENT_SKILL_SUCCESS:
            const parentSkills = state.parentSkills.filter((skill) => skill.id !== payload.parentSkillId);
            return {...state, parentSkills};

        case skillsActionTypes.DELETE_SUB_SKILL_SUCCESS:
            const subSkills = state.subSkills.filter((skill) => skill.id !== payload.subSkillId);
            return {...state, subSkills};

        default:
            return state || {};
    }
};