import {assocPath, dissoc, assoc} from "ramda";
import {employersActionTypes} from "./employersActionTypes";

export const employers = (state, action) => {
    const {payload} = action;

    switch (action.type) {
        case employersActionTypes.GET_EMPLOYERS_PER_PAGE_SUCCESS:
            return payload.employers.reduce((curr, employer) => {
                return {...curr, ...{[employer.id]: employer}};
            }, {});

        case employersActionTypes.GET_SINGLE_EMPLOYER_SUCCESS:
            return assoc(payload.id, payload, state);

        case employersActionTypes.REMOVE_EMPLOYERS: {
            return payload.ids.reduce((currentState, id) => {
                return dissoc(id, currentState);
            }, state);
        }

        case employersActionTypes.ADD_EMPLOYER_SUCCESS:
            return assoc(payload.newEmployer.id, payload.newEmployer, state);

        case employersActionTypes.UPDATE_EMPLOYER_SUCCESS:
            return assoc(payload.id, payload, state);

        default:
            return state || {};
    }
};