import {createAction} from "../services/createAction";
import {employersActionTypes} from "./employersActionTypes";

export const updateEmployer = createAction(employersActionTypes.UPDATE_EMPLOYER);
export const addEmployer = createAction(employersActionTypes.ADD_EMPLOYER);
export const removeEmployers = createAction(employersActionTypes.REMOVE_EMPLOYERS);