import {curryN} from "ramda";

export const createAction = curryN(1, (type, getStaticPayload?) => {
    function action(payload = null, attrs = null) {
        return {
            type,
            payload: getStaticPayload ? getStaticPayload() : payload,
            attrs
        };
    }

    return action;
});