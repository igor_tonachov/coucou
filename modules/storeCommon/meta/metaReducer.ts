import {assocPath, dissoc, assoc, path} from "ramda";
import {metaActionTypes} from "./metaActionTypes";
import {
    employersActionTypes, jobActionTypes, mainActionTypes,
    skillsActionTypes
} from "@StoreCommon";

export const meta = (state, action) => {
    const {payload} = action;

    switch (action.type) {

        case jobActionTypes.GET_EMPLOYER_JOB_PER_PAGE_SUCCESS: {
            let jobs = path(['jobs'], state);
            jobs = {...jobs, ...payload.meta};
            return assocPath(['jobs'], jobs, state);
        }


        case employersActionTypes.GET_EMPLOYERS_PER_PAGE_SUCCESS: {
            let employers = path(['employers'], state);
            employers = {...employers, ...payload.meta};
            return assocPath(['employers'], employers, state);
        }

        case skillsActionTypes.GET_SKILLS_PER_PAGE_SUCCESS: {
            let skills = path(['skills'], state);
            skills = {...skills, ...payload.meta};
            return assocPath(['skills'], skills, state);
        }

        case metaActionTypes.SKILLS_GET_ALL:
            return {...state, ...payload};

        case metaActionTypes.SKILLS_FETCHED:
            return {...state, ...payload};

        case employersActionTypes.GET_ALL_EMPLOYERS_SUCCESS:
            return {...state, employers: {fetched: true}};

        case skillsActionTypes.GET_ALL_SKILLS_SUCCESS:
            return {...state, skills: {fetched: true}};

        case metaActionTypes.EMPLOYERS_FETCHED:
            return {...state, ...payload};

        case metaActionTypes.JOBS_GET_ALL:
            return {...state, ...payload};

        case metaActionTypes.JOBS_FETCHED:
            return {...state, ...payload};

        case mainActionTypes.USER_LOGGIN_ERROR:
            return {...state, login: {error: payload.error}};

        case metaActionTypes.STOCK_IMAGES_REQUEST:
            return {...state, stockImages:payload};

        case metaActionTypes.STOCK_IMAGES_FETCHED:
            return {...state, stockImages:payload};

        default:
            return state || {};
    }
};