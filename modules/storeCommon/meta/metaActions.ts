import {createAction} from "../services/createAction";
import {metaActionTypes} from "./metaActionTypes";

export const skillsGetAll = createAction(metaActionTypes.SKILLS_GET_ALL);
export const skillsFetched = createAction(metaActionTypes.SKILLS_FETCHED);
export const employersGetAll = createAction(metaActionTypes.EMPLOYERS_GET_ALL);
export const employersFetched = createAction(metaActionTypes.EMPLOYERS_FETCHED);
export const jobsGetAll = createAction(metaActionTypes.JOBS_GET_ALL);
export const jobsFetched = createAction(metaActionTypes.JOBS_FETCHED);
export const stockImageRequest = createAction(metaActionTypes.STOCK_IMAGES_REQUEST);
export const stockImageFetched = createAction(metaActionTypes.STOCK_IMAGES_FETCHED);