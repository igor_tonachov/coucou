import * as React from 'react';
import {TableCell, TableRow} from 'material-ui';
import { Rating} from 'material-ui-rating'
import {Delete, ChevronRight, Star} from 'material-ui-icons';
import {Requirement} from "@Interfaces";
import {Languages} from "@Common";

export class RequirementRow extends React.Component<Props, State> {
    render () {
        const {requirement, changeRating, removeRequirement, getSubRequirements} = this.props;
        const rating = requirement.rating || 5;
        return (
            <TableRow key={requirement.id} className="requirement_row">
                <TableCell>
                    <div className="requirement">
                        <If condition={requirement.subRequirements.length}>
                            <ChevronRight className="icon_arrow" />
                        </If>
                        <div className="requirement_name">
                            <span onClick={evt => getSubRequirements(requirement)}>{requirement.title.find(title => title.languageCode === Languages.ENGLISH).content}</span>
                            <Delete className="icon_trash" color="secondary" onClick={evt => removeRequirement(requirement.id)}/>
                        </div>
                    </div>
                </TableCell>
                <TableCell>
                    <div className="rating">
                        <Rating
                            value={rating}
                            max={5}
                            onChange={value =>changeRating(value, requirement.id)}
                            iconFilled={<Star color="secondary" />}
                            iconHovered={<Star color="secondary" />}
                            iconNormal={<Star/>}
                        >
                        </Rating>
                    </div>
                </TableCell>
            </TableRow>
        )
    }
}

interface Props {
    requirement: Requirement;
    changeRating: Function;
    removeRequirement: Function;
    getSubRequirements: Function;
}

interface State {}
