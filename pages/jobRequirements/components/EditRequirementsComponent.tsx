import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {Button, Card, Typography, Grid, Table, TableHead, TableBody, TableCell, TableRow} from 'material-ui';
import { Rating} from 'material-ui-rating'
import {Add, ArrowUpward, KeyboardArrowRight} from 'material-ui-icons';
import * as debounce from 'debounce';

import {
    getSkillsSelector,
    changeQueryString,
    getJobToUpdate,
    addRequirement,
    addSubRequirement,
    removeSelectedJobSkill,
    setCurrentRequirementsList,
    updateRequirement,
    getEmployerJobsList,
    searchChanged} from '@StoreCommon';
import {ActionConfirmationModal} from '@UI';
import {Skill, Job, Requirement} from "@Interfaces";
import {NotificationsService} from "@Sagas";
import {Languages} from "@Common";
import {checkCircularDependencies} from "@Utils";

import {RequirementRow} from './requirementRow/RequirementRowComponent';
import {SkillsList} from "./allSkillsList/AllSkillsListComponent";

import "./jobRequirements.scss";

const con: any = connect;
const withR: any = withRouter;

@withR
@con(store => ({
        skills: getSkillsSelector(store),
        jobToUpdate: getJobToUpdate(store),
        currentRequirements: store.jobs.currentRequirements,
        router: store.router,
        skillsMeta: store.meta.skills,
    }),
    {
        changeQueryString, addRequirement, addSubRequirement, updateRequirement,
        removeSelectedJobSkill, searchChanged, setCurrentRequirementsList, getEmployerJobsList
    })
export class EditRequirementsPage extends React.Component<Props, State> {
    state = {
        isDelete: false,
        requirementToDelete: null,
        path:[],
        jobTitle:[],
        parentRequirement: null,
    };

    componentWillMount() {
        const {jobToUpdate, getEmployerJobsList, router} = this.props;
        const state = router.location.state;
        if(!jobToUpdate) {
            getEmployerJobsList();
        }else if(jobToUpdate && state && state.path) {
            const selectedSkills = this.getRequirementsForCurrentLevel(state.path, jobToUpdate.requirements);
            this.props.setCurrentRequirementsList({currentRequirements:selectedSkills});
            this.setState({jobTitle:state.jobTitle, path:state.path});
        }

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.jobToUpdate !== this.props.jobToUpdate){
            this.setState({jobTitle:[nextProps.jobToUpdate.title]});
        }
    }

    changeRating = (value, requirementId) => {
        const {updateRequirement, jobToUpdate} = this.props;
        const {path} = this.state;
        updateRequirement({jobId:jobToUpdate.id, requirementId, value, path});
    };

    removeRequirement = (id) => {
        this.setState({isDelete:true, requirementToDelete:id});
    };

    confirmDelete = () => {
        const {removeSelectedJobSkill, jobToUpdate} = this.props;
        const {requirementToDelete, path} = this.state;
        removeSelectedJobSkill({jobId:jobToUpdate.id, requirementId:requirementToDelete, path});
        this.setState({isDelete:false, requirementToDelete:""});
    };

    backToRequirementsList = () => {
        this.setState({isDelete:false, requirementToDelete:""});
    };

    cancelEdit = () => {
        const {router, history} = this.props;
        history.push(`/employer/${router.params.employerId}/job/${router.params.jobId}`);
    };

    toogleSkillsModal = () => {
        const {router, history} = this.props;
        const params = router.parsedSearchParams.hasOwnProperty('skillsModal')  ? '' : '?skillsModal=true';
        history.push(`/employer/${router.params.employerId}/job/${router.params.jobId}/requirements${params}`);
    };

    onPageChange = (currentPage) => {
        this.props.changeQueryString({
            params: [
                {
                    parameterName: 'skillsPage',
                    value: currentPage.selected
                }
            ]
        })
    };
    addSkillToJob = (id) => {
        const {addRequirement, jobToUpdate, addSubRequirement} = this.props;
        const {path, parentRequirement} = this.state;
        if(path.length){
            if(checkCircularDependencies(jobToUpdate.requirements, id, path)){
                NotificationsService.addNotification(`Skill won't be added to prevent circular dependencies`, 'error');
                return
            }
            addSubRequirement({jobId: jobToUpdate.id, requirementId:parentRequirement, id, path});
            return;
        }
        addRequirement({jobId: jobToUpdate.id, id, path});
    };

    searchChanged = (value) => {
        this.updateSearch(value);
    };

    updateSearch = debounce((search) => {
        this.props.searchChanged({search});
    }, 300);

    createNewSkill = () => {
        const {history, router} = this.props;
        const {path, jobTitle} = this.state;
        history.push({pathname:`/skill/create`, state: {jobId:router.params.jobId, employerId:router.params.employerId, path, jobTitle}});
    };

    getSubRequirements = (skill) => {
        const title = skill.title.find((t) => t.languageCode === Languages.ENGLISH).content;
        const id = skill.id;
        const path = [...this.state.path, id];
        const jobTitle = [...this.state.jobTitle, title];
        this.props.setCurrentRequirementsList({currentRequirements:skill.subRequirements});
        this.setState({path, jobTitle, parentRequirement:id})
    };

    getTitle = () => {
        const {jobTitle} = this.state;
        return (
            <Typography type="body2" className="job_title">
                <If condition={jobTitle.length > 2}>
                <span className="icon_job_first">
                    <ArrowUpward onClick={this.goToHighestLevel}/>
                </span>
                </If>
                <If condition={jobTitle.length > 1}>
                <span>
                    <span className="text_job_prev" onClick={this.goOneLevelUp}>{jobTitle[jobTitle.length-2]}</span>
                    <span className="icon_job_prev">
                      <KeyboardArrowRight/>
                    </span>
                </span>
                </If>
                <span>
                    {jobTitle[jobTitle.length-1]}
                </span>
            </Typography>
        )
    };

    goToHighestLevel = () => {
        const {jobToUpdate} = this.props;
        this.props.setCurrentRequirementsList({currentRequirements:jobToUpdate.requirements});
        this.setState({path:[], jobTitle:[jobToUpdate.title]});
    };

    goOneLevelUp = () => {
        const {jobToUpdate} = this.props;
        const {jobTitle, path} = this.state;

        jobTitle.length = jobTitle.length - 1;
        path.length = path.length - 1;

        const selectedSkills = this.getRequirementsForCurrentLevel(path, jobToUpdate.requirements);
        this.props.setCurrentRequirementsList({currentRequirements:selectedSkills});
        this.setState({path});
    };

    getRequirementsForCurrentLevel = (path, skills) => {
        if(!path.length) {
            return skills;
        }
        let [first, ...rest] = path;
        if(!rest.length) {
            const s = skills.find(skill => skill.id === first);
            return s.subRequirements
        }else {
            const skill = skills.find(s => s.id === first);
            return this.getRequirementsForCurrentLevel(rest, skill.subRequirements);

        }
    };

    finishEditing = () => {
        const {router, history} = this.props;
        history.push(`/employer/${router.params.employerId}`);
    };

    render () {
        const {skills, skillsMeta, router, currentRequirements} = this.props;
        const {isDelete, path} = this.state;
        const showSkillsModal = router.parsedSearchParams.hasOwnProperty('skillsModal');
        return (
            <div>
            <Card className="requirements_selection">
                <Typography type="title" className="title">
                    Edit required skills
                </Typography>
                {this.getTitle()}
                <Button color="secondary" className="btn_add_skill" onClick={this.toogleSkillsModal}>
                    <Add className="btn_icon"/>
                    add skill
                </Button>
                <Choose>
                    <When condition={currentRequirements.length}>
                        <Grid container className="requirements_list">
                            <Table>
                                <TableHead className="requirements_table_head">
                                    <TableRow>
                                        <TableCell>
                                            <span className="requirement_description">Skill description</span>
                                        </TableCell>
                                        <TableCell>Evaluation</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {currentRequirements.map((requirement) => (
                                        <RequirementRow
                                            key={requirement.id}
                                            requirement={requirement}
                                            removeRequirement={this.removeRequirement}
                                            changeRating={this.changeRating}
                                            getSubRequirements={this.getSubRequirements}
                                        />
                                    ) )}
                                </TableBody>
                            </Table>
                        </Grid>
                    </When>
                    <When condition={!currentRequirements.length && path.length}>
                        <Typography type="caption" className='list_placeholder'>
                            This skill does not have any sub skills
                        </Typography>
                    </When>
                    <Otherwise>
                        <Typography type="caption" className='list_placeholder'>
                            Add required skills for this job
                        </Typography>
                    </Otherwise>
                </Choose>

                <div className="job_btns_group">
                    <Button
                        color="secondary"
                        onClick={this.cancelEdit}
                        className="btn_cancel"
                    >
                        Cancel
                    </Button>
                    <Button
                        raised
                        color="secondary"
                        onClick={this.finishEditing}>
                        Finish
                    </Button>
                </div>
            </Card>
                <If condition={showSkillsModal}>
                    <SkillsList
                        skills={skills}
                        skillsMeta={skillsMeta}
                        onPageChange={this.onPageChange}
                        addSkillToJob={this.addSkillToJob}
                        searchChanged={this.searchChanged}
                        closeSkillsModal={this.toogleSkillsModal}
                        createNewSkill = {this.createNewSkill}
                    />
                </If>
                <If condition={isDelete}>
                    <ActionConfirmationModal
                        title={`You really want to delete this skill and all of its children?`}
                        noAction={this.backToRequirementsList}
                        okAction={this.confirmDelete}
                        yesText="Delete"
                        noText="Cancel"
                    />
                </If>
            </div>
        )
    }
}

interface Props {
    skills?: Skill[];
    currentRequirements?: Requirement[];
    jobToUpdate?: Job;
    skillsMeta?: any;
    history?: any;
    router?: any;
    changeQueryString?: Function;
    updateRequirement?: Function;
    setCurrentRequirementsList?: Function;
    addRequirement?: Function;
    addSubRequirement?: Function;
    removeSelectedJobSkill?: Function;
    getEmployerJobsList?: Function;
    searchChanged?: Function;
}

interface State {
    isDelete: boolean;
    requirementToDelete: string;
    jobTitle: string[];
    path: number[];
    parentRequirement: number;
}
