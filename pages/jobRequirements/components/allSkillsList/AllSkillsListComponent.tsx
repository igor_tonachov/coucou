import * as React from "react";
import {Button, Card, Table, TableHead, TableRow, TableCell, TableBody, Typography, TextField, Grid} from "material-ui";
import {Add} from 'material-ui-icons';
import {remove} from "ramda";

import {Languages} from "@Common";
import {Paginator} from "@UI";
import {Skill} from "@Interfaces";

import "./skillsList.scss";

export class SkillsList extends React.Component <Props, State> {

    state = {
        isOpen: false,
        skillsToRemove: [],
        skillToEdit: null,
        search: "",
        language: Languages.ENGLISH,
        skillsIds:[]
    };

    selectSkills = (id) => {
        const skillsIds = [...this.state.skillsIds, id];
        this.props.addSkillToJob(id);
        this.setState({skillsIds});
    };

    getSkillRows = () => {
        const {skills} = this.props;
        const {skillsIds} = this.state;
        return skills.map((skill, index) => (
            <TableRow key={skill.id} className={skillsIds.indexOf(skill.id) !== -1 ? 'skills_list_item active' : 'skills_list_item'}>
                <TableCell numeric className="skills_short_column">{skill.id}</TableCell>
                <TableCell padding='none' onClick={evt => this.selectSkills(skill.id)}>
                    {skill.title && skill.title.find(title => title.languageCode === this.state.language).content}
                </TableCell>
                <TableCell className="skills_short_column"></TableCell>
            </TableRow>
        ));
    };

    searchChanged = (evt) => {
        this.setState({search: evt.target.value});
        this.props.searchChanged(evt.target.value);
    };

    render() {
        const {skillsMeta, onPageChange, closeSkillsModal, createNewSkill} = this.props;
        const {search} = this.state;

        return (
            <div>
                <div className="overlay" onClick={closeSkillsModal}></div>
                <Card className="job_skills_list">
                        <div className="skills_list_content">
                            <Grid container justify="space-between" spacing={0} className="list_header">
                                <Grid item sm={6}>
                                    <Typography type="title" className="title_table">Skills</Typography>
                                </Grid>
                                <Grid item sm={6} className="skills_search">
                                    <TextField
                                        id="all_skills_list_search"
                                        value={search}
                                        label="Search"
                                        onChange={this.searchChanged}
                                        className="all_skills_list_search"
                                    />
                                </Grid>
                            </Grid>
                            <Table>
                                <TableHead className="table_head">
                                    <TableRow className="table_column_names">
                                        <TableCell className="skills_short_column">Id</TableCell>
                                        <TableCell padding='none'>
                                            Title
                                        </TableCell>
                                        <TableCell className="skills_short_column btn_add">
                                            <Button className="skill_add"
                                                    onClick={createNewSkill}
                                                    color="primary"
                                                    type="submit">
                                                <Add/>
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.getSkillRows()}
                                </TableBody>
                            </Table>
                        </div>
                        <Paginator meta={skillsMeta} onPageChange={onPageChange}/>
                    </Card>
            </div>
        );
    }
}

interface Props {
    skills?: Skill[];
    skillsMeta?: any;
    onPageChange?: Function;
    addSkillToJob: Function;
    history?: any;
    searchChanged: Function;
    createNewSkill: any;
    closeSkillsModal: any;
}

interface State {
    skillsToRemove: any[];
    skillToEdit: Skill;
    search: string;
    language: string;
    skillsIds: number[];
}