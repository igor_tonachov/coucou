import * as React from 'react';
import { withStyles } from 'material-ui/styles';
import {Card, Button, Typography, TextField, Select, MenuItem} from 'material-ui';
import {Check} from 'material-ui-icons';

import {Languages, TranslateApiService} from '@Common';

const styles = theme => ({
    button: {
        marginTop: 20,
        marginBottom: 20,
    },
    input: {
        marginRight: "20%"
    },
    check: {
        fontSize: "18px",
        verticalAlign: "sub",
        marginRight: "3px"
    },
    description: {
        marginBottom: "10px",
        color: "#9E9E9E"
    }
});

const withS: any = withStyles;

@withS(styles)
export class FirstSkillStep extends React.Component <Props, State> {
    state = {
        showTranslateButton: false,
        translatedMessage: false,
        disableTranslateButton: false
    };

    titleChanged = value => {
        const {title} = this.props;
        const titleToChange = title.find(title => title.languageCode == this.props.language);
        let index = null;
        title.forEach((description, i) => {
            if(titleToChange.languageCode === description.languageCode){
                index = i;
            }
        });
        title[index].content = value;
        if (titleToChange.languageCode === Languages.ENGLISH){
            this.props.changeSkill({title: [...title], englishTitle:titleToChange.content});
            return
        }
        this.props.changeSkill({title: [...title]});
    };

    descriptionChanged = value => {
        const {description} = this.props;
        const descriptionToChange = description.find(description => description.languageCode == this.props.language);
        let index = null;
        description.forEach((description, i) => {
            if(descriptionToChange.languageCode === description.languageCode){
                index = i;
            }
        });
        description[index].content = value;
        if (descriptionToChange.languageCode === Languages.ENGLISH){
            this.props.changeSkill({description: [...description], englishDescription:descriptionToChange.content});
            return
        }
        this.props.changeSkill({description: [...description]});
    };

    handleLanguageChange = (e) => {
        const {title, description} = this.props;
        const newLanguage = e.target.value;

        if (!title.find(item => item.languageCode == newLanguage)) {
            this.props.changeSkill({title: [...title, {languageCode: newLanguage, content: ''}]});
        }

        if (!description.find(item => item.languageCode == newLanguage)) {
            this.props.changeSkill({description: [...description, {languageCode: newLanguage, content: ''}]});
        }
        this.props.changeSkill({language: newLanguage});
    };

    translate = () => {
        const {title, description} = this.props;
        const englishTitle = title.find(item => item.languageCode == Languages.ENGLISH);
        const englishDescription = description.find(item => item.languageCode == Languages.ENGLISH);

        this.setState({disableTranslateButton: true});

        const promise1 = TranslateApiService.translate(englishTitle.content).then(resp => {
            this.props.changeSkill({title: [englishTitle, ...resp]});
        });

        const promise2 = TranslateApiService.translate(englishDescription.content).then(resp => {
            this.props.changeSkill({description: [englishDescription, ...resp]});
        });

        Promise.all([promise1, promise2])
            .then(() => {
                this.showSuccessTranslateMsg();
                this.setState({disableTranslateButton: false});
            })
            .catch(() => {
                this.setState({disableTranslateButton: false});
            });
    };

    showSuccessTranslateMsg = () => {
        this.setState({translatedMessage: true}, ()=>{
            setTimeout(()=>{
                this.setState({translatedMessage: false})
            },2000)
        });
    };

    render() {
        const {disableTranslateButton, translatedMessage} = this.state;
        const {classes, cancel, update, isCreating, title, description, languages, language, englishTitle, englishDescription} = this.props;
        const titleContentToChange = title.find(title => title.languageCode == language).content;
        const descriptionToChange = description.find(description => description.languageCode == language).content;
        const headerText = isCreating ? 'Add new skill' : 'Update skill';
        const disabledNextStep = !englishTitle || !englishDescription;

        return (
            <Card
                className="edit_skill"
                aria-labelledby="simple-title"
                aria-describedby="simple-description">
                <div>
                    <div className="edit_skill_content">

                        <Typography type="title" className="title">
                            {headerText}
                        </Typography>
                        <Typography className={classes.description} type="body1">
                            Use english as input text, then hit "Translate"
                            and verify that the translations are correct
                        </Typography>
                        <div className="edit_skill_translate">
                            <TextField
                                id="edit_skill_title"
                                className={classes.input}
                                value={titleContentToChange}
                                label="Title"
                                fullWidth={true}
                                onChange={evt => this.titleChanged(evt.target.value)}
                            />
                            <Select
                                value={language}
                                onChange={this.handleLanguageChange}>
                                {
                                    languages.map((language) => (
                                        <MenuItem value={language.key} key={language.key}>
                                            {language.text}
                                        </MenuItem>
                                    ))
                                }
                            </Select>
                        </div>
                        <TextField
                            id="edit_skill_description"
                            value={descriptionToChange}
                            label="Description"
                            multiline={true}
                            rowsMax={20}
                            onChange={evt => this.descriptionChanged(evt.target.value)}
                        />
                    </div>
                    <If condition={language == Languages.ENGLISH}>
                        <Button
                            className={classes.button}
                            size="small"
                            onClick={this.translate}
                            raised
                            color="secondary"
                            type="submit"
                            disabled={disableTranslateButton || !englishTitle || !englishDescription}>
                            Translate
                        </Button>
                        <If condition={translatedMessage}>
                            <Typography noWrap className="success_text">
                                <Check color="primary" className={classes.check}/> Text successful translated!
                            </Typography>
                        </If>
                    </If>
                    <div className="edit_skill_actions">
                        <Button
                            onClick={cancel}
                            disableRipple={true}
                            color="primary">
                            Cancel
                        </Button>
                        <Button
                            onClick={update}
                            disableRipple={true}
                            color="primary"
                            disabled = {disabledNextStep}
                            >
                            Next
                        </Button>
                    </div>
                </div>
            </Card>
        );
    }
}

interface Props {
    title: { languageCode: string; content: string; }[];
    description: { languageCode: string; content: string; }[];
    skillsMeta?:{pending:boolean, fetched:boolean};
    classes?: any;
    getSkillByIdSelector?: Function;
    history?:History;
    cancel: any;
    update: any;
    isCreating: boolean;
    changeSkill: Function;
    language: string;
    languages: any[];
    englishDescription: string;
    englishTitle: string;
}

interface State {
    showTranslateButton: boolean;
    disableTranslateButton: boolean;
    translatedMessage: boolean;
}