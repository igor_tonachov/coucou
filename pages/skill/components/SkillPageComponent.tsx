import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';

import {Skill, StockImage} from '@Interfaces';
import {getSkillByIdSelector, getSearchResultsSelector, getParentSkillsSelector,
    getSubSkillsSelector, getRelatedSkillsIdsSelector, addSkill, updateSkill,
    fetchStockImages, showSkillStep, searchSkills, saveParentSkill,
    saveSubSkill, getSkillDetails, deleteParentSkill, deleteSubSkill, setCurrentSkillId} from '@StoreCommon';
import {Languages, languagesList} from '@Common';
import {FirstSkillStep} from './firstSkillStep/firstSkillStepComponent';
import {SelectSkillImage} from './selectSkillImage/selectSkillImageComponent';
import {SkillRelations} from './skillRelations/skillRelationsComponent';
import {NotificationsService} from "@Sagas";

import './skillPage.scss';

const withR: any = withRouter;
const con: any = connect;

@withR
@con(store => ({
    skill: getSkillByIdSelector(store),
    skillsMeta: store.meta.skills,
    stockImages: store.stockImages,
    imagesMeta: store.meta.stockImages,
    skillStep: store.skills.skillPage,
    currentSkillId: store.skills.currentSkillId,
    searchResults: getSearchResultsSelector(store),
    parentSkills: getParentSkillsSelector(store),
    subSkills: getSubSkillsSelector(store),
    relatedSkillsIds: getRelatedSkillsIdsSelector(store)

}), {getSkillByIdSelector, updateSkill, addSkill, fetchStockImages,
    showSkillStep, searchSkills, saveParentSkill, saveSubSkill,
    getSkillDetails, deleteParentSkill, deleteSubSkill, setCurrentSkillId})
export class SkillPage extends React.Component <Props, State> {
    state = {
        id: '',
        title: [],
        description: [],
        isCreating: false,
        fileImage: null,
        language: Languages.ENGLISH,
        defaultLanguage: Languages.ENGLISH,
        languages: languagesList,
        imageErrorMessage: '',
        submitted: false,
        recomended: [],
        searched: [],
        stockImageId: null,
        previewImage: "",
        imageToReset: "",
        englishDescription: "",
        englishTitle: "",
        searchHistory: []
    };
    componentWillMount() {
        const {skill, history, setCurrentSkillId} = this.props;
        const isCreating = history.location.pathname.includes('create');

        if (!isCreating && !skill) {
            this.props.history.push('/');
        }

        if (skill) {
            setCurrentSkillId({currentSkillId:skill.id});
            const englishDescription = skill.description.find(description => description.languageCode == Languages.ENGLISH).content;
            const englishTitle = skill.title.find(title => title.languageCode == Languages.ENGLISH).content;
            this.setState({
                id: skill.id,
                title: skill.title,
                description: skill.description,
                previewImage: skill.imageUrl,
                imageToReset: skill.imageUrl,
                englishDescription,
                englishTitle
            });
        }
        else {
            this.setState({
                isCreating,
                title: [{languageCode: Languages.ENGLISH, content: ''}],
                description: [{languageCode: Languages.ENGLISH, content: ''}],
            });
        }
    };

    getStockImages = (queryString?:string) => {
        const {fetchStockImages} = this.props;
        if(queryString) {
            fetchStockImages(queryString);
            return
        }
        const {title} = this.state;
        const englishText = title.find(title => title.languageCode == Languages.ENGLISH).content;
        this.props.fetchStockImages(englishText);
    };

    getImageErrorMessage = (file) => {
        return file ? '' : 'select skill image';
    };

    fileAdded = (image) => {
        this.setState({
            fileImage:image, previewImage:image, stockImageId: null,
            imageErrorMessage: this.getImageErrorMessage(image)
        })
    };

    validate = () => {
        const {isCreating} = this.state;
        const imageErrorMessage = isCreating && this.getImageErrorMessage(this.state.fileImage) || '';
        this.setState({
            imageErrorMessage,
            submitted: true
        });

        return !imageErrorMessage;
    };

    cancel = () => {
        const {state} = this.props.location;
        if(state && state.jobId && state.employerId){
            this.props.history.push({pathname:`/employer/${state.employerId}/job/${state.jobId}/requirements`, state:{path:state.path, jobTitle:state.jobTitle} });
        }else {
            const skillPage = 1;
            this.props.history.push('/');
            this.props.showSkillStep({skillPage});
        }
    };

    updateStep = () => {
        const skillPage = this.props.skillStep + 1;
        this.props.showSkillStep({skillPage});
    };

    prevStep = () => {
        const skillPage = this.props.skillStep - 1;
        this.props.showSkillStep({skillPage});
    };

    goToFinalStep = () => {
        const {searchSkills, getSkillDetails, showSkillStep, skillStep} = this.props;
        const {title, isCreating, id} = this.state;
        const query = title.find(title => title.languageCode == Languages.ENGLISH).content;
        if(isCreating) {
            this.createSkill();
            return;
        }
        const skillPage = skillStep + 1;
        searchSkills({query});
        showSkillStep({skillPage});
        getSkillDetails({id})
    };

    selectStockImage = (selectedStockImage) => {
        this.setState({stockImageId:selectedStockImage.id, previewImage: selectedStockImage.imageUrl})
    };

    setPreviewImage = () => {
        const {fileImage} = this.state;
        if(fileImage){
            this.setState({previewImage: fileImage});
        }
    };

    resetImage = () => {
        this.setState({stockImageId:null, previewImage: this.state.imageToReset});
    };

    createSkill = () => {
        const {title} = this.state;
        if (this.validate()) {
            let data = this.buildSkillData();
            data['query'] = title.find(title => title.languageCode == Languages.ENGLISH).content;
            this.props.addSkill(data);
        }
    };

    saveUpdates = () => {
        if (this.validate()) {
            const data = this.buildSkillData();
            const backRouteData = this.props.location.state;
            data['backRouteData'] = backRouteData;
            this.props.updateSkill(data);
        }
    };

    buildSkillData() {
        const {description, title, fileImage} = this.state;
        const {currentSkillId} = this.props;
        const data = {
            id:currentSkillId,
            description,
            title
        };
        if(this.state.fileImage) {
            data['image'] = fileImage.substring(fileImage.indexOf(',') + 1);
        }
        return data
    }

    changeSkill = (skillProperty) => {
        this.setState(skillProperty)
    };

    saveParentSkill = (skill) => {
        if(this.props.parentSkills.length) {
            for (let s of this.props.parentSkills) {
                if (s.id === skill.id) {
                    NotificationsService.addNotification('Skill is already added to the parent skills', 'info');
                    return;
                }
            }
        }
        const id = this.state.id || this.props.currentSkillId;
        this.props.saveParentSkill({id, parentSkillId:skill.id});
    };

    saveSubSkill = (skill) => {
        if(this.props.subSkills.length) {
            for (let s of this.props.subSkills) {
                if (s.id === skill.id) {
                    NotificationsService.addNotification('Skill is already added to the sub-skills', 'info');
                    return;
                }
            }
        }
        const id = this.state.id || this.props.currentSkillId;
        this.props.saveSubSkill({id, subSkillId: skill.id});
    };

    removeSubSkill = (subSkillId:string) => {
        this.props.deleteSubSkill({id:this.props.currentSkillId, subSkillId})
    };

    removeParentSkill = (parentSkillId:string) => {
        this.props.deleteParentSkill({id:this.props.currentSkillId, parentSkillId})
    };

    searchSkillsByTerm = ({query}) => {
        const {relatedSkillsIds} = this.props;
        this.props.searchSkills({query, excludeSkillIds: relatedSkillsIds});
    };

    render() {
        const {isCreating, title, description,
            language, languages, imageErrorMessage, fileImage,
            recomended, searched, previewImage, stockImageId, englishTitle, englishDescription} = this.state;
        const {stockImages, skillStep, searchResults, imagesMeta, subSkills, parentSkills} = this.props;
        return (
            <div>
                <If condition={skillStep === 1}>
                    <FirstSkillStep
                        englishDescription = {englishDescription}
                        englishTitle = {englishTitle}
                        title={title}
                        description={description}
                        isCreating={isCreating}
                        cancel={this.cancel}
                        update={this.updateStep}
                        changeSkill={this.changeSkill}
                        language={language}
                        languages={languages}
                    />
                </If>
                <If condition={skillStep === 2}>
                    <SelectSkillImage
                        title={englishTitle}
                        language={language}
                        isCreating={isCreating}
                        fileAdded = {this.fileAdded}
                        goToFinalStep={this.goToFinalStep}
                        prevStep={this.prevStep}
                        fetchStockImages={this.getStockImages}
                        fileImage={fileImage}
                        imageErrorMessage={imageErrorMessage}
                        stockImages={stockImages}
                        createSkill={this.createSkill}
                        previewImage={previewImage}
                        selectStockImage = {this.selectStockImage}
                        stockImageId = {stockImageId}
                        resetImage = {this.resetImage}
                        imageMeta={imagesMeta}
                        setPreviewImage = {this.setPreviewImage}
                    />
                </If>
                <If condition={skillStep === 3}>
                    <SkillRelations
                        title={englishTitle}
                        recomended = {recomended}
                        searched = {searched}
                        previewImage={previewImage}
                        saveParentSkill={this.saveParentSkill}
                        saveSubSkill={this.saveSubSkill}
                        parentSkills={parentSkills}
                        subSkills={subSkills}
                        removeParentSkill={this.removeParentSkill}
                        removeSubSkill={this.removeSubSkill}
                        saveUpdates={this.saveUpdates}
                        cancel={this.cancel}
                        searchResults={searchResults}
                        getSearchResults={this.searchSkillsByTerm}
                    />
                </If>
            </div>
        );
    }
}

interface Props {
    skill?: Skill;
    parentSkills?: Skill[];
    subSkills?: Skill[];
    stockImages?: StockImage[],
    skillsMeta?:{pending:boolean, fetched: boolean};
    imagesMeta?:{pending:boolean, fetched: boolean};
    updateSkill?: any;
    addSkill?: any;
    skillStep?: number;
    fetchStockImages?: any;
    showSkillStep?: any;
    getSkillByIdSelector?: Function;
    history?:any;
    currentSkillId?: string;
    searchSkills?:any;
    searchResults?: any[];
    saveParentSkill?: any;
    saveSubSkill?: any;
    getSkillDetails?: any;
    deleteParentSkill?:any;
    deleteSubSkill?:any;
    setCurrentSkillId?:any;
    relatedSkillsIds?: number[];
    location?: any;
}

interface State {
    id: string;
    title: { languageCode: string; content: string; }[];
    description: { languageCode: string; content: string; }[];
    isCreating: boolean;
    fileImage: any;
    language: string;
    languages: any[];
    imageErrorMessage: string;
    submitted: boolean;
    recomended?: string[];
    searched?: string[];
    previewImage: string;
    stockImageId: string;
    imageToReset: string;
    englishDescription: string;
    englishTitle: string;
}

