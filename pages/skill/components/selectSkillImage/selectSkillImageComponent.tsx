import * as React from 'react';
import { withStyles } from 'material-ui/styles';
import {Card, Button, Typography, TextField, GridList, CardMedia, Grid, CircularProgress } from 'material-ui';
import {Photo} from 'material-ui-icons';
import * as debounce from 'debounce';

import {FileUploader} from '@UI';
import {StockImage} from '@Interfaces';

const styles = theme => ({
    input: {
        width: "30%",
        marginBottom: "15px"
    }
});

const withS: any = withStyles;

@withS(styles)
export class SelectSkillImage extends React.Component <Props, State> {
    state = {
        textToSearchImage: this.props.title,
        highlighted: false
    };

    componentWillMount(){
        this.props.fetchStockImages(this.props.title);
    };
    changeSearchText = evt => {
        const {fetchStockImages} = this.props;
        const textToSearchImage = evt.target.value;

        debounce(fetchStockImages(textToSearchImage), 500);
        this.setState({textToSearchImage});
    };

    displaySearchedImages = () => {
        const {stockImages, stockImageId, imageMeta} = this.props;
        if(!imageMeta.pending && imageMeta.fetched) {
            if(stockImages.length) {
                return (
                    stockImages.map(item => (
                        <Grid key={item.id} item>
                            <CardMedia
                                className={(stockImageId === item.id  && !this.state.highlighted) ? 'skill_image active' : 'skill_image'}
                                image={item.thumbnailUrl}
                                onClick={evt => this.setSkillImage(item)}
                            />
                        </Grid>
                    ))
                )
            }
            return (
                <Typography className="no_results" component ="span">
                    No images was found
                </Typography>
            )
        }
        return (
            <CircularProgress  color="secondary" className="loading_stock_images"/>
        )
    };

    setSkillImage = (image) => {
        this.props.selectStockImage(image);
        this.setState({highlighted:false});
    };

    uploadFile = (acceptedFiles) => {
        const file = acceptedFiles && acceptedFiles[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = (e: any) => {
                this.props.fileAdded(e.target.result);
            };
            reader.readAsDataURL(acceptedFiles[0]);
            this.setState({highlighted:true});
        }
    };

    resetImage = () => {
        this.props.resetImage();
        this.setState({highlighted:false});
    };

    hightlight = () => {
        if(!this.props.fileImage) {
            return
        }
        this.props.setPreviewImage();
        this.setState({highlighted:true});
    };

    render() {
        const {classes, isCreating, fileAdded, imageErrorMessage, prevStep,
               fileImage, previewImage, goToFinalStep} = this.props;
        const {textToSearchImage, highlighted} = this.state;
        const headerText = isCreating ? 'Add skill image' : 'Update skill image';

        return (
            <Card
                className="edit_skill_img"
                aria-labelledby="simple-title"
                aria-describedby="simple-description">
                <Typography type="title" className="title">
                    {headerText}
                </Typography>
                <div className="edit_skill_content">
                    <TextField
                        placeholder="Search image"
                        id="edit_skill_title"
                        value={textToSearchImage}
                        className={classes.input}
                        fullWidth={true}
                        onChange={this.changeSearchText}
                    />
                </div>
                <GridList cols={2} cellHeight={'auto'}                                                                                                                                                                                                                                                                                                                   >
                    <div>
                        <div className="images_wrapper">
                            <div className="images_box">
                                <Grid item xs={12} container  justify="space-between"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          >
                                    {this.displaySearchedImages()}
                                </Grid>
                            </div>
                        </div>
                        <div className="upload_wrapper">
                            <Grid className="upload_section">
                                <Grid item xs={6}>
                                    <Typography className="upload_description">
                                        or select file from your computer
                                    </Typography>
                                    <input
                                        accept="image/*"
                                        className="upload_input"
                                        id="raised-button-file"
                                        multiple
                                        type="file"
                                        onChange={(evt) => this.uploadFile(evt.target.files)}
                                    />
                                    <label htmlFor="raised-button-file">
                                        <Button
                                            className="button_upload"
                                            component="span"
                                            raised
                                            color="secondary"
                                            size="small"
                                        >
                                            Upload
                                        </Button>
                                    </label>
                                    <If condition={imageErrorMessage}>
                                        <h4 className="error weight_medium">{imageErrorMessage}</h4>
                                    </If>
                                </Grid>
                                <Grid item xs={6} className={"drop_zone"} onClick={this.hightlight}>
                                    <FileUploader fileAdded={fileAdded} image={fileImage} highlighted={highlighted}/>
                                    <div className="drop_zone_content">
                                        <Photo className="icon_grey"/>
                                        <Typography className={classes.description}>
                                            or drag and drop your image here
                                        </Typography>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                    <div className="selected_image_section">
                        <div className="selected_image_container">
                            <If condition={previewImage}>
                                <img className="preview_image" src={previewImage}/>
                            </If>
                            <If condition={!previewImage}>
                            <div className="no_image_placeholder">
                                <Photo className="icon_grey"/>
                                <Typography className={classes.description}>
                                    Select or upload an image
                                </Typography>
                            </div>
                            </If>
                        </div>
                        <If condition={!isCreating}>
                            <Button
                                disableRipple={true}
                                color="primary"
                                className="reset_image"
                                onClick={this.resetImage}>
                                Reset image
                            </Button>
                        </If>
                    </div>
                </GridList>
                <div className="edit_skill_actions">
                    <Button
                        disableRipple={true}
                        color="primary"
                        onClick={prevStep}>
                        Back
                    </Button>
                    <Button
                        disableRipple={true}
                        className="skill_next_button"
                        color="primary"
                        disabled={isCreating && !fileImage}
                        onClick={goToFinalStep}>
                        Next
                    </Button>
                </div>
            </Card>
        );
    }
}

interface Props {
    title: string;
    language: string;
    classes?: any;
    fileAdded: Function;
    goToFinalStep: any;
    isCreating: boolean;
    imageErrorMessage: string;
    fileImage: string;
    prevStep: any;
    fetchStockImages: Function;
    stockImages:StockImage[];
    createSkill: Function;
    resetImage: Function;
    previewImage: string;
    selectStockImage: Function;
    stockImageId: string;
    imageMeta: {pending:boolean, fetched:boolean};
    setPreviewImage: Function;
}

interface State {
    highlighted:boolean;
    textToSearchImage: string;
}