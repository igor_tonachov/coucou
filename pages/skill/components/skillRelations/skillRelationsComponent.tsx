import * as React from 'react';
import {Card, Button, Typography, Grid } from 'material-ui';
import Input, { InputAdornment } from 'material-ui/Input';
import {Close, ArrowBack, ArrowForward, Delete} from 'material-ui-icons';
import * as debounce from 'debounce';

import {Skill} from '@Interfaces';

export class SkillRelations extends React.Component <Props, State> {
    state = {
        title: this.props.title,
        searchText: this.props.title,
        recomendedActive: null,
        searchedActive: null,
        searchHistory: []
    };

    setSearchText = (evt) => {
        const searchText = evt.target.value ? evt.target.value : "";
        this.setState({searchText});
        if (!searchText) {
            return
        }
        debounce(this.props.getSearchResults({query:searchText}), 500);
    };

    setSearchTerm = (searchText) => {
        this.setState({searchText});
        debounce(this.props.getSearchResults({query:searchText}), 500);
    };

    selectRecomended = (skill) => {
        this.setState({recomendedActive: skill, searchedActive: null})
    };

    selectSearchResult = (skill) => {
        this.setState({searchedActive: skill, recomendedActive: null})
    };

    assignToParent = () => {
        const {saveParentSkill} = this.props;
        const {recomendedActive, searchedActive, searchText} = this.state;
        this.addToSearchHistory(searchText);
        const skill = recomendedActive ? recomendedActive : searchedActive;
        if (!skill) {return}
        saveParentSkill(skill);
        this.setState({recomendedActive:null, searchedActive: null, searchText: ""});
    };

    assignToSubSkills = () => {
        const {saveSubSkill} = this.props;
        const {recomendedActive, searchedActive, searchText} = this.state;
        this.addToSearchHistory(searchText);
        const skill = recomendedActive ? recomendedActive : searchedActive;
        if (!skill) {return}
        saveSubSkill(skill);
        this.setState({recomendedActive:null, searchedActive: null, searchText: ""});
    };

    addToSearchHistory = (searchText) => {
        let {searchHistory} = this.state;
        if(searchHistory.indexOf(searchText) !== -1) {
            return
        }
        searchHistory.push(searchText);
        if(searchHistory.length > 5) {
            searchHistory = searchHistory.slice(Math.max(searchHistory.length - 5, 1));
        }
        this.setState({searchHistory});
    };

    render() {
        const {title, searchText, recomendedActive, searchedActive, searchHistory} = this.state;
        const {
            parentSkills, subSkills, removeParentSkill,
            removeSubSkill, saveUpdates,
            previewImage, cancel, searchResults} = this.props;

        const recentOrResultsText = searchText ? 'Search results' : 'Recent';

        return (
            <div className="skills_relations">
                <Card className="top_control_section">
                    <img className="banner_image" src={previewImage}/>
                    <div className="top_control_content">
                        <Button raised
                                className="top_control_btn btn_cancel"
                                size="small"
                                type="default"
                                onClick={cancel}>
                            Cancel
                        </Button>
                        <Typography type="body2" className="skill_header">
                            {title}
                        </Typography>
                        <Button raised
                                className="top_control_btn btn_finish"
                                size="small"
                                onClick={saveUpdates}>
                            Finish
                        </Button>
                    </div>
                </Card>
                <Grid item xs={12} container justify="center">
                    <Grid item xs={3} className="aside-card-wrapper">
                        <Card className="aside_card">
                            <Typography type="title">
                                Parent skills
                            </Typography>
                            <div className="search_results">
                                {parentSkills.map((skill)=> (
                                    <Typography
                                        key={skill.id}>
                                        {skill.title}
                                        <Delete
                                            className="delete_skill"
                                            onClick={evt => removeParentSkill(skill.id)}
                                        />
                                    </Typography>
                                ))}
                            </div>
                        </Card>
                    </Grid>
                    <Grid item xs={4} className="search-cards-wrapper">
                        <Card
                            className="search_skills_card"
                            aria-labelledby="simple-title"
                            aria-describedby="simple-description">
                            <div className="search_skills">
                                <Input
                                    id="adornment-amount"
                                    value={searchText}
                                    onChange={this.setSearchText}
                                    endAdornment={
                                        <If condition={searchText}>
                                            <InputAdornment position="end">
                                                <Close
                                                    className="clear-input"
                                                    onClick={this.setSearchText}/>
                                            </InputAdornment>
                                        </If>
                                    }
                                />
                            </div>
                            <div className="search_results">
                                <Typography type="body2" className="search_results_title">
                                    {recentOrResultsText}
                                </Typography>
                                <If condition={searchResults.length && searchText}>
                                    {searchResults.map((skill)=> (
                                        <Typography
                                            key={skill.id}
                                            onClick={evt => this.selectSearchResult(skill)}
                                            className = {searchedActive && searchedActive.id === skill.id ? "active" : null}>
                                            {skill.title}
                                        </Typography>
                                    ))}
                                </If>
                                <If condition={searchHistory.length && !searchText}>
                                    {searchHistory.map((term)=> (
                                        <Typography
                                            key={term}
                                            onClick={evt => this.setSearchTerm(term)}>
                                            {term}
                                        </Typography>
                                    ))}
                                </If>
                            </div>
                            <div className="btns_assign">
                                <Button
                                    disableRipple={true}
                                    disabled = {!searchedActive}
                                    onClick={this.assignToParent}>
                                    <ArrowBack className="arrow_back"/>
                                    Assign parent skill
                                </Button>
                                <Button
                                    disableRipple={true}
                                    disabled = {!searchedActive}
                                    onClick={this.assignToSubSkills}>
                                    Assign sub-skill
                                    <ArrowForward className="arrow_forward"/>
                                </Button>
                            </div>
                        </Card>
                        {/*<Card*/}
                            {/*className="recomended_skils"*/}
                            {/*aria-labelledby="simple-title"*/}
                            {/*aria-describedby="simple-description">*/}
                            {/*<Typography type="title">*/}
                                {/*Recomended skills for "{title}"*/}
                            {/*</Typography>*/}
                            {/*<div className="search_results">*/}
                                {/*{results.map((skill)=> (*/}
                                    {/*<Typography*/}
                                        {/*key={skill.id}*/}
                                        {/*className = {recomendedActive === name ? "active" : null}*/}
                                        {/*onClick={evt => this.selectRecomended(skill)}>*/}
                                        {/*{skill.title[0].content}*/}
                                    {/*</Typography>*/}
                                {/*))}*/}
                            {/*</div>*/}
                            {/*<div className="btns_assign">*/}
                                {/*<Button*/}
                                    {/*disableRipple={true}*/}
                                    {/*disabled = {!recomendedActive}*/}
                                    {/*onClick={this.assignToParent}>*/}
                                    {/*<ArrowBack className="arrow_back"/>*/}
                                    {/*Assign parent skill*/}
                                {/*</Button>*/}
                                {/*<Button*/}
                                    {/*disableRipple={true}*/}
                                    {/*disabled = {!recomendedActive}*/}
                                    {/*onClick={this.assignToSubSkills}>*/}
                                    {/*Assign sub-skill*/}
                                    {/*<ArrowForward className="arrow_forward"/>*/}
                                {/*</Button>*/}
                            {/*</div>*/}
                        {/*</Card>*/}
                    </Grid>
                    <Grid item xs={3} className="aside-card-wrapper">
                        <Card className="aside_card">
                            <Typography type="title">
                                Sub-skills
                            </Typography>
                            <div className="search_results">
                                {subSkills.map((skill)=> (
                                    <Typography key={skill.id}>
                                        {skill.title}
                                            <Delete
                                                className="delete_skill"
                                                onClick={evt => removeSubSkill(skill.id)}
                                            />
                                    </Typography>

                                ))}
                            </div>
                        </Card>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

interface Props {
    title: string;
    saveSubSkill: Function;
    saveParentSkill: Function;
    recomended: string[];
    searched: string[];
    parentSkills: Skill[];
    subSkills: Skill[];
    removeParentSkill: Function;
    removeSubSkill: Function;
    saveUpdates: any;
    previewImage: string;
    cancel: any;
    searchResults: any[];
    getSearchResults?: Function;
}

interface State {
    recomendedActive?: Skill;
    searchText:string;
    searchedActive: Skill;
    title:string;
    searchHistory: string[];
}