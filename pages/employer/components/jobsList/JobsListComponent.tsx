import * as React from "react";
import "./jobsList.scss";
import {connect} from "react-redux";
import {getEmployerJobs, removeJobs, getEmployerJobsList} from "@StoreCommon";
import {Job} from "@Interfaces";
import {
    Card,
    Button,
    Checkbox,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    Typography
} from "material-ui";
import {ActionConfirmationModal} from "@UI";
import {Delete, Add, Edit} from 'material-ui-icons';
import {remove} from "ramda";
import {withRouter} from "react-router";
import * as moment from "moment";
import {Paginator} from "../../../../modules/ui/components/paginator/PaginatorComponent";
import {Meta} from "../../../../modules/interfaces/Meta";

const con: any = connect;
const withR: any = withRouter;

@con(store => ({
    jobs: getEmployerJobs(store),
    jobsMeta: store.meta.jobs
}), {removeJobs, getEmployerJobsList})
@withR
export class JobsList extends React.Component <Props, State> {

    state = {
        jobsToRemove: [],
        showConfirmRemove: false
    };

    componentWillMount(){
        this.props.getEmployerJobsList();
    }

    openConfirmRemove = (show) => {
        this.setState({showConfirmRemove: show});
    };

    removeJobs = () => {
        const {jobsToRemove} = this.state;
        const {history, employerId, jobsMeta, jobs} = this.props;

        this.props.removeJobs({ids: jobsToRemove});
        this.setState({
            jobsToRemove: [],
            showConfirmRemove: false
        });

        if (jobs.length == jobsToRemove.length && jobsMeta.number != 0) {
            history.push(`/employer/${employerId}?jobPage=${jobsMeta.number - 1}`);
        }
    };

    toJobView = (id?) => {
        const {history, employerId} = this.props;
        history.push(`/employer/${employerId}/job/${typeof id == "undefined" ? 'create' : id}`);
    };

    toggleSelectedJobs = (id) => {
        const {jobsToRemove} = this.state;
        const index = jobsToRemove.indexOf(id);
        if (index == -1) {
            this.setState({
                jobsToRemove: [...jobsToRemove, id]
            })
        }
        else {
            this.setState({
                jobsToRemove: remove(index, 1, jobsToRemove)
            })
        }
    };

    onPageChange = (currentPage) => {
        const {history, employerId} = this.props;
        history.push(`/employer/${employerId}?jobPage=${currentPage.selected}`);
    };

    render() {
        const {jobs, jobsMeta} = this.props;

        const jobRows = jobs.map((job: Job, index) => (
            <TableRow key={job.id} className="jobs_list_item" onClick={() => this.toggleSelectedJobs(job.id)}>

                <TableCell numeric className="jobs_list_id_column">
                    <Checkbox
                        checked={this.state.jobsToRemove.indexOf(job.id) !== -1}
                        tabIndex={index + 30}
                        disableRipple
                    />
                    <span>{job.id}</span>
                </TableCell>

                <TableCell className="jobs_list_column_title">
                    {job.title}
                </TableCell>

                <TableCell className="jobs_list_column_created_at">
                    {moment(job.createdAt).format("MM.DD.YYYY")}
                </TableCell>

                <TableCell className="job_table_actions_column">
                    <Button
                        onClick={() => this.toJobView(job.id)}
                        color="primary"
                        aria-label="edit">
                        <Edit/>
                    </Button>
                </TableCell>
            </TableRow>
        ));

        return (
            <Card className="jobs_list">
                <div className="jobs_list_content">
                    <Typography type="title" className="title_table">Jobs</Typography>
                    <Table>
                        <TableHead className="table_head">
                            <TableRow className="table_column_names">
                                <TableCell numeric className="jobs_list_id_column">Id</TableCell>
                                <TableCell numeric className="jobs_list_column_title">Title</TableCell>
                                <TableCell className="jobs_list_column_created_at">Created At</TableCell>
                                <TableCell className="job_table_actions_column">
                                    <Button className="jobs_list_header_add"
                                            onClick={() => this.toJobView()}
                                            color="primary"
                                            type="submit">
                                        <Add/>
                                    </Button>
                                    <If condition={this.state.jobsToRemove.length}>
                                        <Button onClick={() => this.openConfirmRemove(true)}
                                                color="secondary"
                                                type="submit">
                                            <Delete/>
                                        </Button>
                                    </If>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody className="table_body">
                            {jobRows}
                        </TableBody>
                    </Table>
                </div>
                <Paginator meta={jobsMeta} onPageChange={this.onPageChange}/>

                <If condition={this.state.showConfirmRemove}>
                    <ActionConfirmationModal
                        title={`You realy want to remove selected jobs?`}
                        noAction={() => this.openConfirmRemove(false)}
                        okAction={this.removeJobs}
                        yesText="Delete"
                        noText="Cancel"
                    />
                </If>
            </Card>
        );
    }
}

interface Props {
    employerId: string;
    jobs?: Job[];
    history?: any;
    removeJobs?: Function;
    jobsMeta?: Meta;
    getEmployerJobsList?: Function;
}

interface State {
    jobsToRemove: any[];
    showConfirmRemove: boolean;
}