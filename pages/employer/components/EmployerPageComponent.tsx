import * as React from "react";
import {connect} from "react-redux";
import {getEmployerToEdit} from "@StoreCommon";
import {Employer} from "@Interfaces";
import {Button, Typography, TextField, Card, FormHelperText, FormControl, InputLabel, Input} from "material-ui";
import "./employerPage.scss";
import {updateEmployer} from "@StoreCommon";
import {JobsList} from "./jobsList/JobsListComponent";
import {isEmailCorrect, phoneNumberChecker} from "@Common";

const con: any = connect;

@con(store => ({
    employer: getEmployerToEdit(store)
}), {updateEmployer})
export class EmployerPage extends React.Component <Props, State> {

    state = {
        employerToEdit: null,
        companyName: '',
        email: '',
        phoneNumber: '',
        submitted: false,
        companyNameError: '',
        emailAddressError: '',
        phoneNumberError: ''
    };

    getCompanyNameError = (companyName) => {
        return !companyName ? 'company name can not be empty' : '';
    };

    getEmailError = (email) => {
        const isEmailError = !email || email && !isEmailCorrect(email);
        return isEmailError ? email ? 'email is not correct' : 'email can not be empty' : '';
    };

    getPhoneError = (phoneNumber) => {
        const isPhoneNumberError = !phoneNumber || phoneNumber && !phoneNumberChecker(phoneNumber);
        return isPhoneNumberError ? phoneNumber ? 'phone is not correct' : 'phone can not be empty' : '';
    };

    setEmployerToUpdate = (employer) => {
        this.setState({
            employerToEdit: employer,
            companyName: employer.companyName,
            email: employer.email,
            phoneNumber: employer.phoneNumber
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.employer && !this.state.employerToEdit) {
            this.setEmployerToUpdate(nextProps.employer);
        }
    }

    componentWillMount() {
        const {employer} = this.props;
        if (employer) {
            this.setEmployerToUpdate(employer);
        }
    }

    companyNameChanged = evt => {
        this.setState({
            companyName: evt.target.value,
            companyNameError: this.getCompanyNameError(evt.target.value)
        });
    };

    emailChanged = evt => {
        this.setState({
            email: evt.target.value,
            emailAddressError: this.getEmailError(evt.target.value)
        });
    };

    phoneChanged = evt => {
        this.setState({
            phoneNumber: evt.target.value,
            phoneNumberError: this.getPhoneError(evt.target.value)
        });
    };

    update = () => {
        const {employerToEdit, companyName, email, phoneNumber} = this.state;

        let emailAddressError = this.getEmailError(email),
            companyNameError = this.getCompanyNameError(companyName),
            phoneNumberError = this.getPhoneError(phoneNumber);

        if (!emailAddressError && !companyNameError && !phoneNumberError) {
            this.props.updateEmployer({
                id: employerToEdit.id,
                companyName,
                email,
                phoneNumber
            });
        } else {
            this.setState({
                submitted: true,
                emailAddressError,
                companyNameError,
                phoneNumberError
            });
        }
    };

    render() {
        const {employerToEdit, companyName, email, phoneNumber, submitted, companyNameError, emailAddressError, phoneNumberError} = this.state;
        return (
            <div className="employer_page">
                <div className="employer_page_content">
                    <Typography type="title" className="title">
                        Edit employer {employerToEdit && employerToEdit.id}
                    </Typography>

                    <If condition={employerToEdit}>
                        <Card className="employer_page_form_container">
                            <form onSubmit={evt => evt.preventDefault()}>

                                <FormControl
                                    error={!!companyNameError && submitted}
                                    aria-describedby="employer_page_company_name_company_name_error">
                                    <InputLabel htmlFor="name-error">Company name</InputLabel>
                                    <Input id="employer_page_company_name_company_name" value={companyName}
                                           onChange={this.companyNameChanged}/>
                                    <If condition={companyNameError && submitted}>
                                        <FormHelperText
                                            id="employer_page_company_name_company_name_error">
                                            {companyNameError}
                                        </FormHelperText>
                                    </If>
                                </FormControl>

                                <FormControl
                                    error={!!emailAddressError && submitted}
                                    aria-describedby="employer_page_company_email_error">
                                    <InputLabel htmlFor="name-error">Email address</InputLabel>
                                    <Input id="employer_page_company_email" value={email}
                                           onChange={this.emailChanged}/>
                                    <If condition={emailAddressError && submitted}>
                                        <FormHelperText
                                            id="employer_page_company_email_error">
                                            {emailAddressError}
                                        </FormHelperText>
                                    </If>
                                </FormControl>

                                <FormControl
                                    error={!!phoneNumberError && submitted}
                                    aria-describedby="employer_page_phone_number_error">
                                    <InputLabel htmlFor="name-error">Phone number</InputLabel>
                                    <Input id="employer_page_phone_number" value={phoneNumber}
                                           onChange={this.phoneChanged}/>
                                    <If condition={phoneNumberError && submitted}>
                                        <FormHelperText
                                            id="employer_page_phone_number_error">
                                            {phoneNumberError}
                                        </FormHelperText>
                                    </If>
                                </FormControl>

                                <Button
                                    raised
                                    color="secondary"
                                    onClick={this.update}>
                                    Update
                                </Button>
                            </form>
                        </Card>
                    </If>

                    <If condition={employerToEdit}>
                        <JobsList employerId={employerToEdit.id}/>
                    </If>
                </div>
            </div>
        );
    }
}

interface Props {
    employer?: Employer;
    updateEmployer?: Function;
}

interface State {
    employerToEdit: Employer;
    companyName: string;
    email: string;
    phoneNumber: string;
    submitted: boolean;
    companyNameError: string;
    emailAddressError: string;
    phoneNumberError: string;
}