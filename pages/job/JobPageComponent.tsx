import * as React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import * as moment from 'moment';
import {Button, Card, FormControl, InputLabel, Input, FormHelperText, Typography, Grid} from 'material-ui';

import {Job} from '@Interfaces'
import {ActionConfirmationModal} from '@UI';
import {addJob, updateJob} from '@StoreCommon';
import {getIsCreating, getJobToUpdate} from "@StoreCommon";

import "./jobPage.scss";

const con: any = connect;
const withR: any = withRouter;

@withR
@con(store => ({
    employerId: store.router && store.router.params && store.router.params.employerId,
    isCreating: getIsCreating(store),
    jobToUpdate: getJobToUpdate(store),
}), {addJob, updateJob})

export class JobPage extends React.Component <Props, State> {

    state = {
        job: null,
        title: '',
        description: '',
        address: '',
        postalCode: '',
        city: '',
        country: '',
        salary: '',
        titleErrorMessage: '',
        descriptionErrorMessage: '',
        addressErrorMessage: '',
        postalCodeErrorMessage: '',
        cityErrorMessage: '',
        countryErrorMessage: '',
        salaryErrorMessage: '',
        submitted: false,
        disableButton: false,
        isConfirmCancel: false
    };

    setupFields = (jobToUpdate) => {
        this.setState({
            job: jobToUpdate,
            title: jobToUpdate.title,
            description: jobToUpdate.description,
            address: `${jobToUpdate.street} ${jobToUpdate.streetNumber}`,
            postalCode: jobToUpdate.postalCode,
            city: jobToUpdate.city,
            country: jobToUpdate.country,
            salary: jobToUpdate.salary
        })
    };

    componentWillMount() {
        const {jobToUpdate, isCreating} = this.props;
        if (jobToUpdate && !isCreating) {
            this.setupFields(jobToUpdate);
            return
        }
        this.setState({disableButton:true});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.jobToUpdate && !this.state.job && !this.props.isCreating) {
            this.setupFields(nextProps.jobToUpdate);
        }
    }

    titleChanged = evt => {
        this.setState({
            title: evt.target.value,
            titleErrorMessage: this.getTitleErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    descriptionChanged = evt => {
        this.setState({
            description: evt.target.value,
            descriptionErrorMessage: this.getDescriptionErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    addressChanged = evt => {
        this.setState({
            address: evt.target.value,
            addressErrorMessage: this.getAddressErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    postalCodeChanged = evt => {
        this.setState({
            postalCode: evt.target.value,
            postalCodeErrorMessage: this.getPostalCodeErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    cityChanged = evt => {
        this.setState({
            city: evt.target.value,
            cityErrorMessage: this.getCityErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    countryChanged = evt => {
        this.setState({
            country: evt.target.value,
            countryErrorMessage: this.getCountryErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    salaryChanged = evt => {
        this.setState({
            salary: evt.target.value,
            salaryErrorMessage: this.getSalaryErrorMessage(evt.target.value)
        }, this.checkFormValidity);
    };

    getTitleErrorMessage = (title) => {
        return title ? '' : 'Title can not be empty';
    };

    getDescriptionErrorMessage = (description) => {
        return description ? '' : 'Description can not be empty';
    };

    getAddressErrorMessage = (address) => {
        return address ? '' : 'Address can not be empty';
    };

    getPostalCodeErrorMessage = (postalCode) => {
        return postalCode ? '' : 'Postal code can not be empty';
    };

    getCityErrorMessage = (city) => {
        return city ? '' : 'City can not be empty';
    };

    getCountryErrorMessage = (country) => {
        return country ? '' : 'Country can not be empty';
    };

    getSalaryErrorMessage = (salary) => {
        return salary ? '' : 'Salary can not be empty';
    };

    checkFormValidity = () => {
        const {title, description, address, postalCode, city, country, salary} = this.state;
        const titleErrorMessage = this.getTitleErrorMessage(title),
            descriptionErrorMessage = this.getDescriptionErrorMessage(description),
            addressErrorMessage = this.getAddressErrorMessage(address),
            postalCodeErrorMessage = this.getPostalCodeErrorMessage(postalCode),
            cityErrorMessage = this.getCityErrorMessage(city),
            countryErrorMessage = this.getCountryErrorMessage(country),
            salaryErrorMessage = this.getSalaryErrorMessage(salary);

        const isErrors = !titleErrorMessage && !descriptionErrorMessage && !addressErrorMessage  &&
            !postalCodeErrorMessage && !cityErrorMessage && !countryErrorMessage && !salaryErrorMessage;

        this.setState({disableButton: !isErrors});
    };

    validate = () => {
        const {title, description, address, postalCode, city, country, salary} = this.state;
        const titleErrorMessage = this.getTitleErrorMessage(title),
            descriptionErrorMessage = this.getDescriptionErrorMessage(description),
            addressErrorMessage = this.getAddressErrorMessage(address),
            postalCodeErrorMessage = this.getPostalCodeErrorMessage(postalCode),
            cityErrorMessage = this.getCityErrorMessage(city),
            countryErrorMessage = this.getCountryErrorMessage(country),
            salaryErrorMessage = this.getSalaryErrorMessage(salary);

        this.setState({
            titleErrorMessage,
            descriptionErrorMessage,
            addressErrorMessage,
            postalCodeErrorMessage,
            cityErrorMessage,
            countryErrorMessage,
            salaryErrorMessage,
            submitted: true
        });

        return !titleErrorMessage && !descriptionErrorMessage && !addressErrorMessage &&
            !postalCodeErrorMessage && !cityErrorMessage && !countryErrorMessage && !salaryErrorMessage;
    };

    create = () => {
        const {employerId} = this.props;
        const {title, description, address, postalCode, city, country, salary} = this.state;
        const streetNumber = address.replace(/\D/g, "");
        const street = address.replace(/\d+/g, '').trim();
        if (this.validate()) {
            this.props.addJob({
                city, country, description, employerId, postalCode, salary, street, title, streetNumber
            });
        }
    };

    cancel = () => {
        const {history} = this.props;
        const {title, description, address, postalCode, city, country, salary} = this.state;
        if (title || description || address || postalCode || city || country || salary) {
            this.setState({isConfirmCancel:true});
        }else {
            history.goBack();
        }
    };

    update = () => {
        const {employerId, jobToUpdate} = this.props;
        const {title, description, address, postalCode, city, country, salary} = this.state;
        const streetNumber = address.replace(/\D/g, "");
        const street = address.replace(/\d+/g, '').trim();
        if (this.validate()) {
            this.props.updateJob({
                ...jobToUpdate,
                city, country, description, employerId,
                postalCode, salary, street, title, streetNumber
            });
        }
    };

    backToEdit = () => {
        this.setState({isConfirmCancel:false})
    };

    confirmCancel = () => {
        const {history, employerId} = this.props;
        history.push(`/employer/${employerId}`);
    };

    render() {
        const {isCreating} = this.props;
        const {
            title, description, address, postalCode, city, country, salary, titleErrorMessage, descriptionErrorMessage,
            addressErrorMessage, postalCodeErrorMessage, cityErrorMessage, countryErrorMessage,
            salaryErrorMessage, submitted, disableButton, job
        } = this.state;

        return (
            <div className="job_edit_form">
                <Card className="job_edit_form_fields">
                    <If condition={!isCreating && job}>
                        <Typography type="title" className="title">
                            Update Job
                        </Typography>
                        <Typography type="caption" className="job_creation_date">
                            Created: {moment(job.createdAt).format("MM.DD.YYYY")}
                        </Typography>
                    </If>

                    <If condition={isCreating}>
                        <Typography type="title" className="title">
                            Create Job
                        </Typography>
                    </If>
                    <Grid container>
                        <Grid item xs={6}>
                            <FormControl
                                margin="dense"
                                error={!!titleErrorMessage && submitted}
                                aria-describedby="job_edit_form_title_error">
                                <InputLabel htmlFor="name-error">Title</InputLabel>
                                <Input id="job_edit_form_title" value={title}
                                       onChange={this.titleChanged}/>
                                <If condition={titleErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_title_error">
                                        {titleErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12}>
                            <FormControl
                                fullWidth
                                margin="dense"
                                error={!!descriptionErrorMessage && submitted}
                                aria-describedby="job_edit_form_description_error">
                                <InputLabel htmlFor="name-error">Description</InputLabel>
                                <Input id="job_edit_form_description" value={description}
                                       onChange={this.descriptionChanged}/>
                                <If condition={descriptionErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_description_error">
                                        {descriptionErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                    </Grid>

                    <Grid container spacing={24}>
                        <Grid item xs={5}>
                            <FormControl
                                margin="dense"
                                error={!!cityErrorMessage && submitted}
                                aria-describedby="job_edit_form_city_error">
                                <InputLabel htmlFor="name-error">City</InputLabel>
                                <Input id="job_edit_form_city" value={city}
                                       onChange={this.cityChanged}/>
                                <If condition={cityErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_city_error">
                                        {cityErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                        <Grid item xs={4}>
                            <FormControl
                                margin="dense"
                                error={!!countryErrorMessage && submitted}
                                aria-describedby="job_edit_form_country_error">
                                <InputLabel htmlFor="name-error">Country</InputLabel>
                                <Input id="job_edit_form_country" value={country}
                                       onChange={this.countryChanged}/>
                                <If condition={countryErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_country_error">
                                        {countryErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl
                                margin="dense"
                                error={!!postalCodeErrorMessage && submitted}
                                aria-describedby="job_edit_form_postal_code_error">
                                <InputLabel htmlFor="name-error">Postal Code</InputLabel>
                                <Input id="job_edit_form_postal_code" value={postalCode} onChange={this.postalCodeChanged}
                                       type="number"/>
                                <If condition={postalCodeErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_postal_code_error">
                                        {postalCodeErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <FormControl
                                fullWidth
                                margin="dense"
                                error={!!addressErrorMessage && submitted}
                                aria-describedby="job_edit_form_country_error">
                                <InputLabel htmlFor="name-error">Address</InputLabel>
                                <Input id="job_edit_form_country" value={address}
                                       onChange={this.addressChanged}/>
                                <If condition={addressErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_country_error">
                                        {addressErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <FormControl
                                fullWidth
                                margin="dense"
                                error={!!salaryErrorMessage && submitted}
                                aria-describedby="job_edit_form_salary_error">
                                <InputLabel htmlFor="name-error">Salary</InputLabel>
                                <Input id="job_edit_form_salary" value={salary}
                                       onChange={this.salaryChanged} type="number"/>
                                <If condition={salaryErrorMessage && submitted}>
                                    <FormHelperText
                                        id="job_edit_form_salary_error">
                                        {salaryErrorMessage}
                                    </FormHelperText>
                                </If>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <div className="job_btns_group">
                        <Choose>
                            <When condition={isCreating}>
                                <Button
                                    color="secondary"
                                    onClick={this.cancel}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    color="secondary"
                                    onClick={this.create}
                                    disabled={disableButton}>
                                    Next
                                </Button>
                            </When>
                            <Otherwise>
                                <Button
                                    color="secondary"
                                    onClick={this.confirmCancel}>
                                    Cancel
                                </Button>
                                <Button
                                    color="secondary"
                                    onClick={this.update}>
                                    Next
                                </Button>
                            </Otherwise>
                        </Choose>
                    </div>

                </Card>
                <If condition={this.state.isConfirmCancel}>
                    <ActionConfirmationModal
                        title={`You have unsaved progress. Do yo really want to cancel?`}
                        noAction={this.backToEdit}
                        okAction={this.confirmCancel}
                        yesText="Ok"
                        noText="Cancel"
                    />
                </If>
            </div>
        );
    }
}

interface Props {
    isCreating?: boolean;
    jobToUpdate?: Job;
    addJob?: Function;
    employerId?: string;
    updateJob?: Function;
    history?: any;
}

interface State {
    job: Job;
    title: string;
    description: string;
    address: string;
    postalCode: any;
    city: string;
    country: string;
    salary: string;
    titleErrorMessage: string;
    descriptionErrorMessage: string;
    addressErrorMessage: string;
    postalCodeErrorMessage: string;
    cityErrorMessage: string;
    countryErrorMessage: string;
    salaryErrorMessage: string;
    submitted: boolean;
    disableButton: boolean;
    isConfirmCancel: boolean;
}