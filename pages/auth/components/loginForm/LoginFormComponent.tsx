import * as React from "react";
import {Card, TextField, Typography, CardContent, CardActions, Button} from "material-ui";
import "./loginForm.scss";
import {connect} from "react-redux";
import {userLogin} from "@StoreCommon";
import {withRouter} from "react-router";
import axios from 'axios';
import {basePath} from "@Common";

const con: any = connect;
const withR: any = withRouter;

@withR
@con(null, {userLogin})
export class LoginForm extends React.Component <Props, State> {

    state = {
        login: '',
        password: '',
        errorMessage: ''
    };

    loginChanged = evt => {
        this.setState({login: evt.target.value});
    };

    passwordChanged = evt => {
        this.setState({password: evt.target.value});
    };

    login = evt => {
        evt.preventDefault();

        this.setState({errorMessage: ''});

        axios.post(`${basePath}/login?username=${this.state.login}&password=${this.state.password}`)
            .then((resp: any) => {
                this.props.userLogin({user: {}});
            })
            .catch(resp => {
                const message = resp && resp.response && resp.response.data && resp.response.data.message;

                if (message) {
                    this.setState({errorMessage: message})
                }
            });
    };

    render() {
        return (
            <form className="login_form" onSubmit={this.login}>
                <Card className="login_form_wrapper">
                    <CardContent className="login_form_content">
                        <If condition={this.state.errorMessage}>
                            <h4 className="error weight_medium">{this.state.errorMessage}</h4>
                        </If>

                        <Typography type="display1" className="login_form_header">
                            Login
                        </Typography>

                        <TextField
                            id="login_form_login_input"
                            className="login_form_input"
                            value={this.state.login}
                            label="Login"
                            onChange={this.loginChanged}
                        />

                        <TextField
                            id="login_form_password_input"
                            className="login_form_input"
                            value={this.state.password}
                            label="Password"
                            onChange={this.passwordChanged}
                        />
                    </CardContent>
                    <CardActions className="login_form_actions">
                        <Button raised color="primary" type="submit">Sign in</Button>
                    </CardActions>
                </Card>
            </form>
        );
    }
}

interface Props {
    userLogin?: Function;
    history?: any;
}

interface State {
    login: string;
    password: string;
    errorMessage: string;
}