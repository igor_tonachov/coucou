import * as React from "react";
import {Route, Switch} from "react-router-dom";
import {withRouter} from "react-router";
import {pageNameAliases} from "@Common";
import {LoginForm} from "./loginForm/LoginFormComponent";
import "./auth.scss";

const withR: any = withRouter;

@withR
export class Auth extends React.Component <Props, {}> {
    render() {
        return (
            <div className="auth">
                <div className="auth_form">
                    <Switch>
                        <Route path={pageNameAliases.login} component={LoginForm}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

interface Props {
}