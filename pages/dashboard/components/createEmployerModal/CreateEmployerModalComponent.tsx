import * as React from "react";
import {Modal, Button, Typography, FormControl, InputLabel, Input, FormHelperText} from "material-ui";
import "./createEmployerModal.scss";
import {isEmailCorrect, phoneNumberChecker} from "../../../../modules/common/services/validations";

export class CreateEmployerModal extends React.Component <Props, State> {

    state = {
        companyName: '',
        email: '',
        phoneNumber: '',
        submitted: false,
        companyNameError: '',
        emailAddressError: '',
        phoneNumberError: ''
    };

    getCompanyNameError = (companyName) => {
        return !companyName ? 'company name can not be empty' : '';
    };

    getEmailError = (email) => {
        const isEmailError = !email || email && !isEmailCorrect(email);
        return isEmailError ? email ? 'email is not correct' : 'email can not be empty' : '';
    };

    getPhoneError = (phoneNumber) => {
        const isPhoneNumberError = !phoneNumber || phoneNumber && !phoneNumberChecker(phoneNumber);
        return isPhoneNumberError ? phoneNumber ? 'phone is not correct' : 'phone can not be empty' : '';
    };

    companyNameChanged = (evt) => {
        this.setState({
            companyName: evt.target.value,
            companyNameError: this.getCompanyNameError(evt.target.value)
        });
    };

    emailAddressChanged = (evt) => {
        this.setState({
            email: evt.target.value,
            emailAddressError: this.getEmailError(evt.target.value)
        });
    };

    phoneNumberChanged = (evt) => {
        this.setState({
            phoneNumber: evt.target.value,
            phoneNumberError: this.getPhoneError(evt.target.value)
        });
    };

    create = () => {
        const {companyName, email, phoneNumber} = this.state;
        let emailAddressError = this.getEmailError(email),
            companyNameError = this.getCompanyNameError(companyName),
            phoneNumberError = this.getPhoneError(phoneNumber);

        if (!emailAddressError && !companyNameError && !phoneNumberError) {
            this.props.createEmployer({
                companyName,
                email,
                phoneNumber
            });
        } else {
            this.setState({
                submitted: true,
                emailAddressError,
                companyNameError,
                phoneNumberError
            });
        }
    };

    render() {
        const {cancelAction} = this.props;
        const {email, companyName, phoneNumber, submitted, companyNameError, emailAddressError, phoneNumberError} = this.state;

        return (
            <Modal
                className="create_employer_modal modal"
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={true}
                onClose={cancelAction}>
                <div>
                    <div className="create_employer_modal_content">
                        <Typography type="title" className="title">
                            Create Employer
                        </Typography>

                        <FormControl
                            error={!!companyNameError && submitted}
                            aria-describedby="create_employer_modal_company_name_error">
                            <InputLabel htmlFor="name-error">Company name</InputLabel>
                            <Input id="create_employer_modal_company_name" value={companyName}
                                   onChange={this.companyNameChanged}/>
                            <If condition={companyNameError && submitted}>
                                <FormHelperText
                                    id="create_employer_modal_company_name_error">
                                    {companyNameError}
                                </FormHelperText>
                            </If>
                        </FormControl>

                        <FormControl
                            error={!!emailAddressError && submitted}
                            aria-describedby="create_employer_modal_email_error">
                            <InputLabel htmlFor="name-error">Email address</InputLabel>
                            <Input id="create_employer_modal_email" value={email}
                                   onChange={this.emailAddressChanged}/>
                            <If condition={emailAddressError && submitted}>
                                <FormHelperText
                                    id="create_employer_modal_email_error">
                                    {emailAddressError}
                                </FormHelperText>
                            </If>
                        </FormControl>

                        <FormControl
                            error={!!phoneNumberError && submitted}
                            aria-describedby="create_employer_modal_phone_number_error">
                            <InputLabel htmlFor="name-error">Phone number</InputLabel>
                            <Input id="create_employer_modal_phone_number" value={phoneNumber}
                                   onChange={this.phoneNumberChanged}/>
                            <If condition={phoneNumberError && submitted}>
                                <FormHelperText
                                    id="create_employer_modal_phone_number_error">
                                    {phoneNumberError}
                                </FormHelperText>
                            </If>
                        </FormControl>
                    </div>

                    <div className="create_employer_modal_actions">
                        <Button
                            raised
                            color="secondary"
                            onClick={this.create}>
                            Create
                        </Button>

                        <Button
                            raised
                            color="primary"
                            onClick={cancelAction}>
                            Cancel
                        </Button>
                    </div>
                </div>
            </Modal>
        );
    }
}

interface Props {
    cancelAction: any;
    createEmployer: any;
}

interface State {
    companyName: string;
    email: string;
    phoneNumber: string;
    submitted: boolean;
    companyNameError: string;
    emailAddressError: string;
    phoneNumberError: string;
}