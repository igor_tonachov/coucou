import * as React from "react";
import "./dashboardPage.scss";
import {EmployersList} from "./employersList/EmployersListComponent";
import {SkillsList} from "./skillsList/SkillsListComponent";
import {withRouter} from "react-router";

const withR: any = withRouter;

@withR
export class DashboardPage extends React.Component <Props, State> {

    render() {
        return (
            <div className="dashboard_page">
                <div className="content">
                    <EmployersList/>
                    <SkillsList/>
                </div>
            </div>
        );
    }
}

interface Props {
}

interface State {
}