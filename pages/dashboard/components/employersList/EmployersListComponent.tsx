import * as React from "react";
import "./employersList.scss";
import {Button, Card, Checkbox, Table, TableHead, TableRow, TableCell, TableBody, Typography} from "material-ui";
import {Delete, Edit, Add} from 'material-ui-icons';
import {findIndex, propEq, remove} from "ramda";
import {ActionConfirmationModal} from "@UI";
import {Employer} from "@Interfaces";
import {connect} from "react-redux";
import {getEmployersSelector, removeEmployers, changeQueryString, addEmployer} from "@StoreCommon";
import {withRouter} from "react-router";
import {Paginator} from "../../../../modules/ui/components/paginator/PaginatorComponent";
import {Meta} from "@Interfaces";
import {CreateEmployerModal} from "../createEmployerModal/CreateEmployerModalComponent";

const con: any = connect;
const withR: any = withRouter;

@withR
@con(store => ({
    employers: getEmployersSelector(store),
    employersMeta: store.meta.employers,
    routerParams: store.router && store.router.parsedSearchParams,
}), {removeEmployers, changeQueryString, addEmployer})
export class EmployersList extends React.Component <Props, State> {

    state = {
        employersToRemove: [],
        showConfirmRemove: false,
        showCreateEmployer: false
    };

    openConfirmRemove = (show) => {
        this.setState({showConfirmRemove: show});
    };

    removeEmployers = () => {
        const {employersToRemove} = this.state;
        const {employers, employersMeta, removeEmployers, changeQueryString} = this.props;

        removeEmployers({ids: employersToRemove});
        this.setState({
            employersToRemove: [],
            showConfirmRemove: false
        });

        if (employers.length == employersToRemove.length && employersMeta.number != 0) {
            changeQueryString({
                params: [
                    {
                        parameterName: 'employersPage',
                        value: employersMeta.number - 1
                    }
                ]
            });
        }
    };

    editEmployer = (id) => {
        this.props.history.push(`/employer/${id}`);
    };

    toggleSelectedEmployer = (id) => {
        const {employersToRemove} = this.state;
        const index = employersToRemove.indexOf(id);
        if (index == -1) {
            this.setState({
                employersToRemove: [...employersToRemove, id]
            })
        }
        else {
            this.setState({
                employersToRemove: remove(index, 1, employersToRemove)
            })
        }
    };

    onPageChange = (currentPage) => {
        this.props.changeQueryString({
            params: [
                {
                    parameterName: 'employersPage',
                    value: currentPage.selected
                }
            ]
        });
    };

    createEmployer = (newEmployer) => {
        this.props.addEmployer({newEmployer});
        this.showCreateEmployer(false);
    };

    showCreateEmployer = (show) => {
        this.setState({
            showCreateEmployer: show
        });
    };

    render() {
        const {employers, employersMeta} = this.props;
        const {showCreateEmployer} = this.state;

        const employersRows = employers.map((employer, index) => (
            <TableRow key={employer.id} className="employers_list_item"
                      onClick={() => this.toggleSelectedEmployer(employer.id)}>

                <TableCell>
                    <Checkbox
                        checked={this.state.employersToRemove.indexOf(employer.id) !== -1}
                        tabIndex={index + 10}
                        disableRipple
                    />
                </TableCell>

                <TableCell numeric>
                    {employer.id}
                </TableCell>

                <TableCell>
                    {employer.companyName}
                </TableCell>

                <TableCell className="employers_table_actions_column">
                    <div>
                        <Button
                            className="employer_edit"
                            onClick={() => this.editEmployer(employer.id)}
                            color="primary"
                            aria-label="edit">
                            <Edit/>
                        </Button>
                    </div>
                </TableCell>
            </TableRow>
        ));

        return (
            <Card className="employers_list">

                <div className="employers_list_content">
                    <Typography type="title" className="title_table">Employers</Typography>
                    <Table>
                        <TableHead className="table_head">
                            <TableRow className="table_column_names">
                                <TableCell></TableCell>
                                <TableCell numeric>Id</TableCell>
                                <TableCell numeric>Company Name</TableCell>
                                <TableCell className="employers_table_actions_column">
                                    <div>
                                        <If condition={this.state.employersToRemove.length}>
                                            <Button onClick={() => this.openConfirmRemove(true)}
                                                    color="secondary"
                                                    type="submit">
                                                <Delete/>
                                            </Button>
                                        </If>
                                        <Button className="employer_add"
                                                onClick={() => this.showCreateEmployer(true)}
                                                color="primary"
                                                type="submit">
                                            <Add/>
                                        </Button>
                                    </div>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {employersRows}
                        </TableBody>
                    </Table>
                </div>

                <Paginator meta={employersMeta} onPageChange={this.onPageChange}/>

                <If condition={this.state.showConfirmRemove}>
                    <ActionConfirmationModal
                        title={`You realy want to remove selected employers?`}
                        noAction={() => this.openConfirmRemove(false)}
                        okAction={this.removeEmployers}
                        yesText="Delete"
                        noText="Cancel"
                    />
                </If>

                <If condition={showCreateEmployer}>
                    <CreateEmployerModal
                        cancelAction={() => this.showCreateEmployer(false)}
                        createEmployer={this.createEmployer}
                    />
                </If>
            </Card>
        );
    }
}

interface Props {
    employers?: Employer[];
    removeEmployers?: Function;
    history?: any;
    employersMeta?: Meta;
    routerParams?: any;
    changeQueryString?: Function;
    addEmployer?: Function;
}

interface State {
    employersToRemove: any[];
    showConfirmRemove: boolean;
    showCreateEmployer: boolean;
}