import * as React from "react";
import "./skillsList.scss";
import {getSkillsSelector, removeSkills, changeQueryString} from "@StoreCommon";
import {connect} from "react-redux";
import {Skill} from "@Interfaces";
import {Button, Card, Checkbox, Table, TableHead, TableRow, TableCell, TableBody, Typography} from "material-ui";
import {ActionConfirmationModal} from "@UI";
import {Delete, Edit, Add} from 'material-ui-icons';
import {remove} from "ramda";
import {Languages} from "@Common";
import {Paginator} from "@UI";
import {withRouter} from "react-router";

const withR: any = withRouter;
const con: any = connect;

@withR
@con(store => ({
    skills: getSkillsSelector(store),
    skillsMeta: store.meta.skills
}), {removeSkills, changeQueryString})
export class SkillsList extends React.Component <Props, State> {

    state = {
        skillsToRemove: [],
        skillToEdit: null,
        showConfirmRemove: false,
        language: Languages.ENGLISH,
    };

    openConfirmRemove = (open) => {
        this.setState({showConfirmRemove: open});
    };

    removeSkills = () => {
        const {skillsToRemove} = this.state;
        const {removeSkills, skills, skillsMeta, changeQueryString} = this.props;

        removeSkills({ids: skillsToRemove});
        this.setState({
            skillsToRemove: [],
            showConfirmRemove: false
        });

        if (skills.length == skillsToRemove.length && skillsMeta.number != 0) {
            changeQueryString({
                params: [
                    {
                        parameterName: 'skillsPage',
                        value: skillsMeta.number - 1
                    }
                ]
            });
        }
    };

    startEditSkill = (evt, skill) => {
        this.props.history.push(`/skill/${skill.id}`);
    };

    toggleSelectedSkills = (id) => {
        const {skillsToRemove} = this.state;
        const index = skillsToRemove.indexOf(id);
        if (index == -1) {
            this.setState({
                skillsToRemove: [...skillsToRemove, id]
            })
        }
        else {
            this.setState({
                skillsToRemove: remove(index, 1, skillsToRemove)
            })
        }
    };

    onPageChange = (currentPage) => {
        this.props.changeQueryString({
            params:[
                {
                    parameterName: 'skillsPage',
                    value: currentPage.selected
                }
            ]
        })
    };

    showCreateSkill = () => {
        this.props.history.push(`/skill/create`);
    };

    render() {
        const {skills, skillsMeta} = this.props;
        const { skillsToRemove, showConfirmRemove} = this.state;

        const skillsRows = skills.map((skill, index) => (
            <TableRow key={skill.id} className="skills_list_item" onClick={() => this.toggleSelectedSkills(skill.id)}>
                <TableCell>
                    <Checkbox
                        checked={this.state.skillsToRemove.indexOf(skill.id) !== -1}
                        tabIndex={index + 20}
                        disableRipple
                    />
                </TableCell>
                <TableCell numeric>{skill.id}</TableCell>
                <TableCell>
                    {skill.title && skill.title.find(title => title.languageCode == this.state.language).content}
                </TableCell>

                <TableCell className="skills_table_action_column">
                    <div>
                        <Button
                            className="skill_edit"
                            onClick={evt => this.startEditSkill(evt, skill)}
                            color="primary"
                            aria-label="edit">
                            <Edit/>
                        </Button>
                    </div>
                </TableCell>
            </TableRow>
        ));

        return (
            <Card className="skills_list">
                <div className="skills_list_content">
                    <Typography type="title" className="title_table">Skills</Typography>
                    <Table>
                        <TableHead className="table_head">
                            <TableRow className="table_column_names">
                                <TableCell></TableCell>
                                <TableCell numeric>Id</TableCell>
                                <TableCell>Title</TableCell>
                                <TableCell className="skills_table_action_column">
                                    <div>
                                        <If condition={skillsToRemove.length}>
                                            <Button onClick={() => this.openConfirmRemove(true)}
                                                    color="secondary"
                                                    type="submit">
                                                <Delete/>
                                            </Button>
                                        </If>
                                        <Button className="skill_add"
                                                onClick={() => this.showCreateSkill()}
                                                color="primary"
                                                type="submit">
                                            <Add/>
                                        </Button>
                                    </div>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {skillsRows}
                        </TableBody>
                    </Table>
                </div>

                <Paginator meta={skillsMeta} onPageChange={this.onPageChange}/>

                <If condition={showConfirmRemove}>
                    <ActionConfirmationModal
                        title={`You realy want to remove selected skills?`}
                        noAction={() => this.openConfirmRemove(false)}
                        okAction={this.removeSkills}
                        yesText="Delete"
                        noText="Cancel"
                    />
                </If>
            </Card>
        );
    }
}

interface Props {
    skills?: Skill[];
    removeSkills?: Function;
    skillsMeta?: any;
    changeQueryString?: Function;
    history?: any;
}

interface State {
    skillsToRemove: any[];
    showConfirmRemove: boolean;
    skillToEdit: Skill;
    language: string;
}