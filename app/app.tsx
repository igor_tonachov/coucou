import * as React from "react";
import {Provider} from 'react-redux';
import {ConnectedRouter, routerMiddleware} from 'react-router-redux';
import {createStore, applyMiddleware, compose} from "redux";
import createHistory from 'history/createBrowserHistory';
import createSagaMiddleware from 'redux-saga';
import {detect} from "detect-browser";
import {Routes} from "./routes";
import rootReducers from "./store/rootReducer";
import {initialState} from "./store/initialState";
import {rootSaga} from "@Sagas";
import "./styles/site.scss";

const history = createHistory();
const sagaMiddleware = createSagaMiddleware();

const middlewares: any = [
    routerMiddleware(history),
    sagaMiddleware
];

let composeEnhancers = compose;

if (process.env.NODE_ENV === 'development') {
    if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
    }
}

export const store: any = createStore(
    rootReducers,
    initialState,
    composeEnhancers(applyMiddleware(...middlewares))
);

sagaMiddleware.run(rootSaga);

const browser = detect();
document.body.classList.add("is_" + browser.name);

export class App extends React.Component<{}, {}> {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Routes/>
                </ConnectedRouter>
            </Provider>
        );
    }
}