import {combineReducers} from 'redux';
import {employers, jobs, main, router, skills, meta, stockImages} from "@StoreCommon";
import {jobPage} from "../../modules/storeCommon/jobPage/jobPageReducer";

const rootReducers = combineReducers({
    main,
    router,
    skills,
    jobs,
    jobPage,
    employers,
    meta,
    stockImages
});

export default rootReducers;