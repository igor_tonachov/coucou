import {Languages} from "@Common";

export const initialState = {
    main: {
        locale: {
            name: Languages.ENGLISH,
            pending: true
        },
        user: localStorage.getItem('login') ? {data: {}} : null,
        login: localStorage['login']
    },
    stockImages: [],
    skills: {
        skillsList: {},
        skillPage: 1,
        currentSkillId: null,
        searchResults: [],
        subSkills: [],
        parentSkills: [],
        jobId: null,
        employerId: null
    },
    employers: {},
    jobs: {
        jobsList: {},
        currentRequirements: []
    },
    jobPage: {},
    meta: {
        stockImages: {
            pending: false,
            fetched: false
        },
        skills: {
            pending: false,
            fetched: false
        },
        employers: {
            pending: false,
            fetched: false
        },
        jobs: {
            pending: false,
            fetched: false
        }
    }
};