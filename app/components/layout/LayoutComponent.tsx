import * as React from "react";
import {TopBar} from "@UI";
import * as NotificationSystem from 'react-notification-system'
import {NotificationsService} from "@Sagas";

const style = {
    NotificationItem: {
        success: {
            backgroundColor: 'white',
            boxShadow: '0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
            borderTop: '2px solid #00897b'
        },
        error: {
            backgroundColor: 'white',
            boxShadow: '0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
            borderTop: '2px solid #F52223'
        }
    }
};

export class Layout extends React.Component <Props, {}> {

    notificationSystemRef;
    notificationsUnsubscribe;

    componentDidMount() {

        this.notificationsUnsubscribe = NotificationsService.subscribe((text, type) => {
            this.notificationSystemRef.addNotification({
                message: text,
                level: type,
                dismissible: false
            });
        })
    }

    componentWillUnmount() {
        this.notificationsUnsubscribe && this.notificationsUnsubscribe();
    }

    render() {
        return (
            <div className="layout">
                <TopBar/>
                {this.props.children}
                <NotificationSystem ref={ref => this.notificationSystemRef = ref} style={style}/>
            </div>
        );
    }
}

interface Props {
    children?: any;
}