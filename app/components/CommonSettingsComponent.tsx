import * as React from "react";
import {withRouter} from "react-router";
import {connect} from "react-redux";
import {availableLocales, getUserLocale} from "@Common";
import {getWindowDimensions} from "../utils/dimensionUtils";
import * as debounce from 'debounce';
import {changeLocale, setWindowDimensions} from "@StoreCommon";
import {NavigationService} from "@Sagas";

const withR: any = withRouter;
const con: any = connect;

@con(state => ({
    locale: state.main.locale.name,
    localePending: state.main.locale.pending
}), {changeLocale, setWindowDimensions})
@withR
export default class CommonSettings extends React.Component<props, {}> {

    unsubscribeNavigation;

    setLocale() {
        let locale = getUserLocale(availableLocales);
        this.props.changeLocale({localeName: 'en'});
    }

    componentWillMount() {
        this.setLocale();
        window.addEventListener('resize', debounce(this.setWindowDimensions, 350));
        this.setWindowDimensions();
        this.unsubscribeNavigation = NavigationService.subscribe((to) => {
            this.props.history.push(to);
        })
    }

    componentWillUnmount() {
        this.unsubscribeNavigation && this.unsubscribeNavigation();
    }

    setWindowDimensions = () => {
        this.props.setWindowDimensions({dimensions: getWindowDimensions()});
    };

    render() {
        const {localePending, children} = this.props;
        return localePending ? null : children;
    }
}

interface props {
    history?: any;
    children?: any;
    changeLocale?: any;
    locale?: string;
    localePending?: boolean;
    setWindowDimensions?: any;
}