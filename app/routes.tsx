import * as React from "react";
import {BrowserRouter, Route, NavLink, IndexRoute, Switch, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import CommonSettings from "./components/CommonSettingsComponent";
import {withRouter} from "react-router";
import {authPageNames, PageNames} from "@Common";
import {Auth} from "@Auth";
import {NoMatch} from "./components/NoMatchComponent";
import {DashboardPage} from "@Dashboard";
import {EmployerPage} from "@EmployerPage";
import {Layout} from "./components/layout/LayoutComponent";
import {JobPage} from "@JobPage";
import {SkillPage} from "@SkillPage";
import {EditRequirementsPage} from "@JobEditSkillsPage";

const withR: any = withRouter;
const con: any = connect;

@con(store => ({
    user: store.main.user
}))
@withR
class LoggedInZone extends React.Component<any, any> {
    render() {
        const {user, children} = this.props;
        return (
            <If condition={user.data}>
                <div className="logged_in_zone">
                    {children}
                </div>
            </If>
        );
    }
}

@con((store) => ({
    isAuthenticated: !!store.main.login,
    activePageName: store.router.activePageName
}))
@withR
export class Routes extends React.Component<RoutesProps, {}> {

    render() {

        const {isAuthenticated, activePageName} = this.props;

        if (!isAuthenticated) {
            if (authPageNames.indexOf(activePageName) == -1) {
                return (<Redirect to="/login"/>);
            }

            return (
                <CommonSettings>
                    <Auth/>
                </CommonSettings>
            )
        }

        if (authPageNames.indexOf(activePageName) != -1) {
            return (<Redirect to="/"/>);
        }

        //Todo: remove it, when route will fixed
        let page;

        switch (activePageName) {
            case PageNames.DASHBOARD:
                page = <DashboardPage/>;
                break;
            case PageNames.EMPLOYER_UPDATE:
                page = <EmployerPage/>;
                break;
            case PageNames.JOB_UPDATE:
            case PageNames.JOB_CREATE:
                page = <JobPage/>;
                break;
            case PageNames.SKILL_UPDATE:
            case PageNames.SKILL_CREATE:
                page = <SkillPage/>;
                break;
            case PageNames.EDIT_REQUIREMENTS:
                page = <EditRequirementsPage/>;
                break;
        }

        if (page) {
            return (
                <CommonSettings>
                    <LoggedInZone>
                        <Layout>
                            {page}
                        </Layout>
                    </LoggedInZone>
                </CommonSettings>
            )
        }

        //Todo: fix it later(problems with rerender pages on navigation change)
        // if (pageNameAliases[activePageName]) {
        //     return (
        //         <CommonSettings>
        //             <Switch>
        //                 <LoggedInZone>
        //                     <Switch>
        //                         <Route path="/employer" render={() => (
        //                             <Switch>
        //                                 <Route exact path={pageNameAliases.employerCreate} component={EmployerPage}/>
        //                                 <Route path={pageNameAliases.employerUpdate} component={EmployerPage}/>
        //                             </Switch>
        //                         )}/>
        //                         <Route path={pageNameAliases.dashboard} component={DashboardPage}/>
        //                     </Switch>
        //                 </LoggedInZone>
        //             </Switch>
        //         </CommonSettings>
        //     );
        // }

        return (
            <CommonSettings>
                <Route path="*" component={NoMatch}/>
            </CommonSettings>
        )
    }
}

interface RoutesProps {
    isAuthenticated?: boolean;
    activePageName?: string;
    history?: any;
}