import * as React from "react";
import * as ReactDOM from "react-dom";
import {App} from "./app";
import * as moment from "moment";
import {MuiThemeProvider, createMuiTheme} from 'material-ui/styles';

moment.locale("en");

const theme = createMuiTheme({
    palette: {
        primary: {main: '#00897b'},
        secondary: {main: '#00897b'},
    },
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <App />
    </MuiThemeProvider>
    ,
    document.getElementById("app")
);