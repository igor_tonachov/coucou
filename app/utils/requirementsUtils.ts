export const addRequiremet = (requirements, requirement, path) => {
    let [first, ...rest] = path;
    if(!first) {
        requirements.push(requirement);
        return
    }else if(!rest.length) {
        const skill = requirements.find(s => s.id === first);
        skill.subRequirements.push(requirement);
        return
    }else {
        const sub = requirements.find(r => r.id === first);
        return addRequiremet(sub.subRequirements, requirement, rest);
    }
};

export const removeRequiremet = (requirements, requirementId, path) => {
    let [first, ...rest] = path;
    if(!first) {
        for(let i=0; i<requirements.length; i++){
            if(requirements[i].id == requirementId){
                requirements.splice(i, 1);
            }
        }
    }else {
        const requirement = requirements.find(r => r.id === first);
        return removeRequiremet(requirement.subRequirements, requirementId, rest) ;
    }
};

export const  checkCircularDependencies = (requirements, skillId, path) => {
    let [first, ...rest] = path;
    const list = requirements.find(r => r.skillId === skillId);
    if(list) {
        return true;
    }
   else if(!first) {
       return false;
   }else {
        const requirement = requirements.find(r => r.id === first);
        return checkCircularDependencies(requirement.subRequirements, skillId, rest);
    }

};

export const  updateRequirementRating = (requirements, requirementId, value, path) => {
    let [first, ...rest] = path;
    if(!first) {
        for(let i=0; i<requirements.length; i++){
            if(requirements[i].id == requirementId){
                requirements[i].rating = value;
                return
            }
        }
    } else {
        const requirement = requirements.find(r => r.id === first);
        return updateRequirementRating(requirement.subRequirements, requirementId, value, rest);
    }
};