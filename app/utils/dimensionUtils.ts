const getNodeDimensions = (node: HTMLElement | any = document.createElement('div')): IDimensions => {
    return {
        width: node.clientWidth || node.innerWidth,
        height: node.clientHeight || node.innerHeight
    };
};

export const BREAKPOINTS = {

    DESKTOP6: 2560,
    DESKTOP5: 1920,
    DESKTOP4: 1600,
    DESKTOP3: 1440,
    DESKTOP2: 1280,
    DESKTOP1: 1024,
    TABLET: 990,
    MOBILE: 767,
    SMALL_MOBILE: 479,
    CARD_SIZE: 640
};

export const getWindowDimensions = getNodeDimensions.bind(null, window);