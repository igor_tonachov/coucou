import CleanWebpackPlugin from 'clean-webpack-plugin';
import path from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import DefinePlugin from 'webpack/lib/DefinePlugin';
import HtmlWebpackExcludeAssets from 'html-webpack-exclude-assets-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import webpack from "webpack";
import fs from 'fs';

const {CheckerPlugin, TsConfigPathsPlugin} = require('awesome-typescript-loader');

function parse(path) {
    return JSON.parse(fs.readFileSync(path, 'utf8'));
};

const envFilePath = process.env.ENV_FILE || "./env.json";
const projectSettings = parse(envFilePath);

module.exports = {
    entry: ["babel-polyfill", "./app/index.tsx"],
    output: {
        path: path.join(__dirname, "../dist/"),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['babel-loader', 'awesome-typescript-loader']
            },
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)/,
                loader: 'file-loader'
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    plugins: [
        new webpack.optimize.ModuleConcatenationPlugin(),
        new CheckerPlugin(),
        new HtmlWebpackPlugin({
            template: './app/index.html',
            excludeAssets: /theme_/
        }),
        new HtmlWebpackExcludeAssets(),
        new CleanWebpackPlugin(['dist'], {
            root: path.join(__dirname, '../'),
            verbose: true
        }),
        new DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                'SETTINGS': JSON.stringify(projectSettings),
            }
        }),
        new CopyWebpackPlugin([
            {from: './resources', to: 'static'}
        ])
    ],
    resolve: {
        extensions: ['.tsx', '.ts', '.jsx', '.js'],
        modules: ['node_modules'],
        plugins: [new TsConfigPathsPlugin()]
    }
};