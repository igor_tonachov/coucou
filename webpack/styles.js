import ExtractTextPlugin from 'extract-text-webpack-plugin';

const isProduction = process.env.NODE_ENV === 'production';

const moduleRules = isProduction ?
    [
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                allChunks: true,
                use: ['css-loader', 'autoprefixer-loader', 'sass-loader']
            })
        },
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                allChunks: true,
                use: ['css-loader', 'autoprefixer-loader']
            })
        }
    ] :
    [
        {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'autoprefixer-loader', 'sass-loader']
        },
        {
            test: /\.css$/,
            use: ['style-loader', 'css-loader', 'autoprefixer-loader']
        }
    ];

module.exports = {
    module: {
        rules: moduleRules
    },
    plugins: [
        new ExtractTextPlugin('[name].css')
    ],
    resolve: {
        extensions: ['.css']
    }
};