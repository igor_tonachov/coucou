import webpack from 'webpack';
import webpackMerge from 'webpack-merge';
import commonConfig from './common.js';
import stylesConfig from "./styles";
import DefinePlugin from 'webpack/lib/DefinePlugin';

module.exports = webpackMerge(commonConfig, stylesConfig, {
    devtool: 'source-map',
    output: {
        filename: '[name].[chunkhash].js'
    },
    plugins: [
        new DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            mangle: {screw_ie8: true},
            compress: {screw_ie8: true, warnings: false},
            comments: false,
            screw_ie8: true
        })
    ]
});