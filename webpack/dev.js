import commonConfig from './common.js';
import webpackMerge from 'webpack-merge';
import stylesConfig from "./styles";
import DefinePlugin from 'webpack/lib/DefinePlugin';

module.exports = webpackMerge(commonConfig, stylesConfig, {
    output: {
        filename: "[name].js"
    },
    plugins: [
    ],
    devtool: "cheap-module-source-map",
    devServer: {
        stats: 'minimal',
        port: 9999,
        inline: true,
        historyApiFallback: true
    }
});