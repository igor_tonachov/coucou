# README #

### What is this repository for? ###

This is the frontend repository of the coucou job platform.

### How do I get set up? ###

* Clone this repository
* Get ssh access and run `git submodule update --init` to also get the REST library as a submodule
* Copy the env_example.json to env.json - it includes the current URL of the dev server (don't commit the env.json - it will be automatically added within the deployment pipeline)
* Run `npm install`
* Run `npm run dev`

### How do I contribute? ###

* We use git flow (see https://danielkummer.github.io/git-flow-cheatsheet)
* Create feature branches for issues (e.g. "feature/CCS-129-user_settings")
* Link the related issue(s) in your commit description (e.g. "CCS-129 #done Implemented user settings")
* Create pull request for merges into the develop branch

### Who do I talk to? ###

* Contact david.beck@tapdo.io