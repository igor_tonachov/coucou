declare var process: any;

interface IDimensions {
    width: number;
    height: number;
}

declare module 'material-ui/TextField' {
    const id: string;
    const value: string;
    const label: string;
    const onChange: Function;
}


interface Window {
    t?: any;
}